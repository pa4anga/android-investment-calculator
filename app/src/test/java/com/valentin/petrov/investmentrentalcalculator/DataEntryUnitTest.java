package com.valentin.petrov.investmentrentalcalculator;

import android.widget.TextView;

import org.junit.Test;

import static com.valentin.petrov.dataentry.DataEntryActivity.hasInvalidInput;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DataEntryUnitTest {

    @Test
    public void checkInputNullIsInvalid() {

        TextView mock = mock(TextView.class);
        assertTrue(hasInvalidInput(mock));
    }

    @Test
    public void checkInputEmptyIsInvalid() {

        TextView mock = mock(TextView.class);
        when(mock.getText()).thenReturn("");
        assertTrue(hasInvalidInput(mock));
    }

    @Test
    public void checkInputSpaceIsInvalid() {

        TextView mock = mock(TextView.class);
        when(mock.getText()).thenReturn("  ");
        assertTrue(hasInvalidInput(mock));
    }

    @Test
    public void checkInputValidManyWordIsValid() {

        TextView mock = mock(TextView.class);
        when(mock.getText()).thenReturn("  well shit   /#$%^^&");
        assertFalse(hasInvalidInput(mock));
    }

    @Test
    public void checkInputValidSingleWordIsValid() {

        TextView mock = mock(TextView.class);
        when(mock.getText()).thenReturn("Cheese");
        assertFalse(hasInvalidInput(mock));
    }
}