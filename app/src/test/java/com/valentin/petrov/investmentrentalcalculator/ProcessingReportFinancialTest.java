package com.valentin.petrov.investmentrentalcalculator;

import com.valentin.petrov.database.FullReport;

import org.junit.Test;

import static com.valentin.petrov.displayreport.ProcessedReport.calculateLoanInterestOnlyPayment;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateMaintenanceCosts;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateManagementCosts;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateMonthlyLoanPayment;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateReturnOnInvestment;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateTotalCashNeeded;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateVacancyCosts;
import static com.valentin.petrov.displayreport.ProcessedReport.getEquity;
import static junit.framework.TestCase.assertEquals;



public class ProcessingReportFinancialTest {

    @Test
    public void checkMaintenanceCostCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 5000;
        mockReport.maintenance = 25;
        assertEquals(1250.0,calculateMaintenanceCosts(mockReport));
    }

    @Test
    public void checkMaintenanceCostCalculationUneven(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 12123;
        mockReport.maintenance = 21;
        assertEquals(2545.83,calculateMaintenanceCosts(mockReport));
    }

    @Test
    public void checkManagementCostCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 5000;
        mockReport.management = 25;
        assertEquals(1250.0,calculateManagementCosts(mockReport));
    }

    @Test
    public void checkManagementCostCalculationUneven(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 12123;
        mockReport.management = 21;
        assertEquals(2545.83,calculateManagementCosts(mockReport));
    }

    @Test
    public void checkVacancyCostCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 5000;
        mockReport.vacancy = 25;
        assertEquals(1250.0,calculateVacancyCosts(mockReport));
    }

    @Test
    public void checkVacancyCostCalculationUneven(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 12123;
        mockReport.vacancy = 21;
        assertEquals(2545.83,calculateVacancyCosts(mockReport));
    }

    @Test
    public void checkTotalCashNeededCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 50000;
        mockReport.lenderCharges = 2500;
        mockReport.loanAmount= 5000;
        mockReport.intoLoanR = false;
        mockReport.pointCharges = 500;

        assertEquals(48000.0, calculateTotalCashNeeded(mockReport));
    }

    @Test
    public void checkTotalCashNeededCalculationNegative(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 500;
        mockReport.lenderCharges = 2500;
        mockReport.loanAmount= 5000;
        mockReport.intoLoanR = false;
        mockReport.pointCharges = 500;

        assertEquals(0.0, calculateTotalCashNeeded(mockReport));
    }
    @Test
    public void checkTotalCashNeededCalculationWrappingCharges(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 50000;
        mockReport.lenderCharges = 2500;
        mockReport.loanAmount= 5000;
        mockReport.intoLoanR = true;
        mockReport.pointCharges = 500;

        assertEquals(45000.0, calculateTotalCashNeeded(mockReport));
    }

    @Test
    public void checkTotalCashNeededCalculationWrappingChargesNegative(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 500;
        mockReport.lenderCharges = 2500;
        mockReport.loanAmount= 5000;
        mockReport.intoLoanR = true;
        mockReport.pointCharges = 500;

        assertEquals(0.0, calculateTotalCashNeeded(mockReport));
    }

    @Test
    public void checkManagementExpensesCalculationUneven(){
        FullReport mockReport = new FullReport();
        mockReport.purchasePrice= 12123;
        mockReport.management = 21;
        assertEquals(2545.83,calculateManagementCosts(mockReport));
    }

    @Test
    public void checkInterestCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.loanAmount= 5000;
        mockReport.intoLoanR = false;
        mockReport.interestRate = 5;

        assertEquals(20.83, calculateLoanInterestOnlyPayment(mockReport));
    }

    @Test
    public void checkInterestCalculationUneven(){
        FullReport mockReport = new FullReport();
        mockReport.loanAmount= 123123;
        mockReport.intoLoanR = false;
        mockReport.interestRate = 8;

        assertEquals(820.82, calculateLoanInterestOnlyPayment(mockReport));
    }

    @Test
    public void checkReturnOnInvestmentCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.afterRepairValue= 70000;
        mockReport.purchasePrice = 50000;
        mockReport.repairCosts = 5000;

        assertEquals(27.27, calculateReturnOnInvestment(mockReport));
    }

    @Test
    public void checkReturnOnInvestmentCalculationFull(){
        FullReport mockReport = new FullReport();
        mockReport.afterRepairValue= 70000;
        mockReport.purchasePrice = 50000;
        mockReport.repairCosts = 5000;
        mockReport.closingCosts = 1000;

        assertEquals(25.0, calculateReturnOnInvestment(mockReport));
    }

    @Test
    public void checkReturnOnInvestmentCalculationWithoutLoan(){
        FullReport mockReport = new FullReport();
        mockReport.afterRepairValue= 70000;
        mockReport.purchasePrice = 50000;
        mockReport.repairCosts = 5000;
        mockReport.closingCosts = 1000;
        mockReport.cashPurchase = true;
        mockReport.lenderCharges = 5000;

        assertEquals(25.0, calculateReturnOnInvestment(mockReport));
    }

    @Test
    public void checkReturnOnInvestmentCalculationWrappedCharges(){
        FullReport mockReport = new FullReport();
        mockReport.afterRepairValue= 70000;
        mockReport.purchasePrice = 50000;
        mockReport.repairCosts = 5000;
        mockReport.closingCosts = 1000;
        mockReport.intoLoanR = true;
        mockReport.lenderCharges = 5000;

        assertEquals(25.0, calculateReturnOnInvestment(mockReport));
    }

    @Test
    public void checkReturnOnInvestmentCalculationWithLoan(){
        FullReport mockReport = new FullReport();
        mockReport.afterRepairValue= 70000;
        mockReport.purchasePrice = 50000;
        mockReport.repairCosts = 5000;
        mockReport.closingCosts = 1000;
        mockReport.cashPurchase = false;
        mockReport.lenderCharges = 5000;
        mockReport.pointCharges = 5000;
        mockReport.intoLoanR = false;

        assertEquals(6.06, calculateReturnOnInvestment(mockReport));
    }

    @Test
    public void checkReturnOnInvestmentCalculationNegative(){
        FullReport mockReport = new FullReport();
        mockReport.afterRepairValue= 134;
        mockReport.purchasePrice = 5000;
        mockReport.repairCosts = 250;

        assertEquals(-97.45, calculateReturnOnInvestment(mockReport));
    }



    @Test
    public void  checkLoanPaymentCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.loanAmount= 5000;
        mockReport.intoLoanR = false;
        mockReport.interestRate = 5;
        mockReport.interestOnly = false;
        mockReport.amortizationRate = 30;

        assertEquals(27.10, calculateMonthlyLoanPayment(mockReport));
    }

    @Test
    public void  checkLoanPaymentCalculationInterestOnly(){
        FullReport mockReport = new FullReport();
        mockReport.loanAmount= 5000;
        mockReport.intoLoanR = false;
        mockReport.interestRate = 5;
        mockReport.interestOnly = true;
        mockReport.amortizationRate = 30;

        assertEquals(20.83, calculateMonthlyLoanPayment(mockReport));
    }

    @Test
    public void  checkLoanPaymentCalculationLarge(){
        FullReport mockReport = new FullReport();
        mockReport.loanAmount= 75000;
        mockReport.intoLoanR = false;
        mockReport.interestRate = 7;
        mockReport.interestOnly = false;
        mockReport.amortizationRate = 30;

        assertEquals(503.66, calculateMonthlyLoanPayment(mockReport));
    }

    @Test
    public void  checkLoanPaymentCalculationZeroInterest(){
        FullReport mockReport = new FullReport();
        mockReport.loanAmount= 75000;
        mockReport.intoLoanR = false;
        mockReport.interestRate = 0;
        mockReport.interestOnly = false;
        mockReport.amortizationRate = 30;

        assertEquals(208.33, calculateMonthlyLoanPayment(mockReport));
    }

    @Test
    public void  checkEquityCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.loanAmount= 75000;
        mockReport.afterRepairValue = 80000;

        assertEquals(5000., getEquity(mockReport));
    }

    //TODO Finish
    public void checkChartDataCalculation(){
        FullReport mockReport = new FullReport();
        mockReport.cashPurchase = false;
        mockReport.purchasePrice = 500000;
        mockReport.afterRepairValue = 550000;
        mockReport.closingCosts = 5000;
        mockReport.repairCosts = 5000;

        mockReport.loanAmount = 450000;
        mockReport.lenderCharges = 1000;
        mockReport.interestRate = 4;
        mockReport.pointCharges = 0;

        mockReport.amortizationRate = 25;

        mockReport.electricity = 200;
        mockReport.water = 150;
        mockReport.sewer = 100;
        mockReport.garbage = 100;
        mockReport.hoa = 0;
        mockReport.insurance = 800;

        mockReport.propertyTax = 300;

        mockReport.rent = 500;
        mockReport.otherIncome = 500;


    }
}
