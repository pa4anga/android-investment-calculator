package com.valentin.petrov.database;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;

import androidx.lifecycle.LiveData;

import com.valentin.petrov.dataentry.ReportEditor;
import com.valentin.petrov.dataentry.ReportInserter;

import java.io.File;
import java.lang.ref.WeakReference;
import java.net.URI;
import java.util.List;

public class ReportsRepository {

    private static volatile ReportsRepository instance;
    private static ReportDAO reportDAO;
    private static FullReportDAO fullReportDAO;
    private LiveData<List<MainMenuReport>> reports;
    private LiveData<List<FullReport>> fullReports;
    private static LoanDAO loanDAO;

    private ReportsRepository(ReportsDB db) {
        fullReportDAO = db.fullReportDAO();
        reportDAO = db.reportDAO();
        this.reports=reportDAO.loadAllDisplayReports();
        this.fullReports=fullReportDAO.loadAllFullReports();
        loanDAO=db.loanDAO();
    }

    public static ReportsRepository getInstance(Application app) {
        if (instance == null) {
            synchronized (ReportsRepository.class) {
                instance = new ReportsRepository(ReportsDB.getDatabase(app));
            }
        }
        return instance;
    }

    public void insertFullReport(FullReport fullReport,ReportInserter inserter){
        new InsertOneFullReportTask(inserter).execute(fullReport);
    }

    public void loadReportById(long id, ReportEditor receiver) {
        new FetchOneFullReportForEditingTask(receiver).execute(id);
    }

    private static class FetchOneFullReportForEditingTask extends AsyncTask<Long, Integer, FullReport> {

        private WeakReference<ReportEditor> recieverReference;

        FetchOneFullReportForEditingTask(ReportEditor receiver){
            this.recieverReference = new WeakReference<>(receiver);
        }

        @Override //ONLY EVER USE FOR ONE REPORT AT A TIME
        protected FullReport doInBackground(Long... id) {
            return fullReportDAO.loadFullReportByIdSync(id[0]);
        }

        protected void onPostExecute(FullReport result) {
            recieverReference.get().onEditReportRetrieved(result);
        }
    }

    private static class DeleteOneFullReportByIdTask extends AsyncTask<Long, Integer, Integer> {
        @Override //ONLY EVER USE FOR ONE ID AT A TIME
        protected Integer doInBackground(Long... id) {
            Report report = reportDAO.loadReportByIdSync(id[0]);
            if(reportDAO.getNumberOfReportsUsingImage(report.imageURI)==0){
                URI imageUri = URI.create(report.imageURI);
                File imageFile = new File(imageUri);
                imageFile.delete();
            }
            return reportDAO.deleteReportById(id[0]);
        }
    }

    private static class UpdateOneFullReportTask extends AsyncTask<FullReport, Integer, Long> {
        private WeakReference<ReportInserter> inserterReference;

        UpdateOneFullReportTask(ReportInserter inserter){
            this.inserterReference = new WeakReference<>(inserter);
        }
        @Override //ONLY EVER USE FOR ONE ID AT A TIME
        protected Long doInBackground(FullReport... fullReports) {
            Report databaseReport = reportDAO.loadReportByIdSync(fullReports[0].id);
            Log.d("DB","Database report" + databaseReport);
            if(fullReports[0].imageURI!=null&&
                    fullReports[0].imageURI.equals(databaseReport.imageURI)){//clean image storage
                if(reportDAO.getNumberOfReportsUsingImage(databaseReport.imageURI)==0){
                    URI imageUri = URI.create(databaseReport.imageURI);
                    File imageFile = new File(imageUri);
                    imageFile.delete();
                }
            }
            //deal with loan
            if(!databaseReport.cashPurchase){
                if(fullReports[0].cashPurchase){
                    loanDAO.deleteLoanById(databaseReport.id);
                }
                else{
                    Log.d("DB", "Database report "+databaseReport);
                    Log.d("DB", "Loan with id  "+databaseReport.loanId);
                    Loan updateLoan = fullReports[0].convertToLoan();
                    updateLoan.id = databaseReport.loanId;
                    Log.d("DB", "Loan with id  " + updateLoan.id + " updated");
                    loanDAO.updateLoan(updateLoan);
                    fullReports[0].loanId=databaseReport.loanId;
                }
            }
            else {
                if (!fullReports[0].cashPurchase){
                    long loanId=loanDAO.insertLoan(fullReports[0].convertToLoan());
                    Log.d("DB", "Loan with id  " + loanId + " created");
                    fullReports[0].loanId=loanId;
                }
            }
            Report updateReport = fullReports[0].convertToReport();
            updateReport.id = databaseReport.id;
            //update report
            reportDAO.updateReport(updateReport);
            Log.d("DB", "Report with id  " + updateReport.id + " updated");
            return databaseReport.id;
        }

        protected void onPostExecute(Long result) {
            inserterReference.get().onReportInserted(result);
        }

    }

    private static class InsertOneFullReportTask extends AsyncTask<FullReport, Integer, Long> {

        private WeakReference<ReportInserter> inserterReference;

        InsertOneFullReportTask(ReportInserter inserter){
            this.inserterReference = new WeakReference<>(inserter);
        }

        @Override //ONLY EVER USE FOR ONE REPORT AT A TIME
        protected Long doInBackground(FullReport... fullReports) {
            Long loanId = null;
            if (!fullReports[0].cashPurchase) {
                loanId=loanDAO.insertLoan(fullReports[0].convertToLoan());
                Log.d("DB", "Loan with id  " + loanId + " created");
            }
            fullReports[0].loanId=loanId;

            long id=reportDAO.insertReport(fullReports[0].convertToReport());
            Log.d("DB", "Report with id  " + id + " created");//debug

            return id;
        }

        protected void onPostExecute(Long result) {
            inserterReference.get().onReportInserted(result);
        }
    }

    private static class CheckIfIdExists extends AsyncTask<Long, Integer, Integer> {
        @Override //ONLY EVER USE FOR ONE ID AT A TIME
        protected Integer doInBackground(Long... id) {
            if(fullReportDAO.checkIfReportExists(id[0])==0)throw new IllegalArgumentException("ID not in Database");

            return 0;

        }
    }
    public void updateFullReport(FullReport report,ReportInserter inserter){
        new UpdateOneFullReportTask(inserter).execute(report);
    }

    public void checkIfIdExists(long id){
        new CheckIfIdExists().execute(id);
    }

    public void deleteReportById(long id){
        new DeleteOneFullReportByIdTask().execute(id);
    }

    public LiveData<List<MainMenuReport>> getDisplayReports() {
        return reports;
    }

    public LiveData<List<FullReport>> getFullReports() {
        return fullReports;
    }

    public LiveData<FullReport> getFullReportById(long id){
        return fullReportDAO.loadFullReportById(id);
    }
}
