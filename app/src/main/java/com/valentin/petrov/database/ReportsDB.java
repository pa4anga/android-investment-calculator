package com.valentin.petrov.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

@Database(entities = {Report.class, Loan.class}, views = {FullReport.class}, version = 1)
public abstract class ReportsDB extends RoomDatabase {

    public abstract LoanDAO loanDAO();
    public abstract ReportDAO reportDAO();
    public abstract FullReportDAO fullReportDAO();

    private static volatile ReportsDB instance;

    static ReportsDB getDatabase(final Context context) {
        if (instance == null) {
            synchronized (ReportsDB.class) {
                if (instance == null) {
                    instance = Room.databaseBuilder(context.getApplicationContext(),
                            ReportsDB.class, "reports_db")
                            .build();
                }
            }
        }
        return instance;
    }
}
