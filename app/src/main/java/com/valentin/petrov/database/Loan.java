package com.valentin.petrov.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

@Entity(indices = {@Index(value = {"id"},
        unique = true)})
public class Loan {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long id;

    @ColumnInfo(name = "interest_only")
    public boolean interestOnly;

    @ColumnInfo(name = "loan_amount")
    public long loanAmount;

    @ColumnInfo(name = "lender_charges")
    public int lenderCharges;

    @ColumnInfo(name = "point_charges")
    public int pointCharges;

    @ColumnInfo(name = "interest_rate")
    public double interestRate;

    @ColumnInfo(name = "into_loan")
    public boolean intoLoanR;

    @ColumnInfo(name = "amortization_rate")
    public int amortizationRate;

    public Loan (){};

    @Ignore
    public Loan(long id, boolean interestOnly, long loanAmount,
                int lenderCharges, int pointCharges, double interestRate,
                boolean intoLoanR, int amortizationRate) {
        this.id = id;
        this.interestOnly = interestOnly;
        this.loanAmount = loanAmount;
        this.lenderCharges = lenderCharges;
        this.pointCharges = pointCharges;
        this.interestRate = interestRate;
        this.intoLoanR = intoLoanR;
        this.amortizationRate = amortizationRate;
    }
}
