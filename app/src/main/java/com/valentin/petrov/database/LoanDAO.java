package com.valentin.petrov.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface LoanDAO {

    @Query("SELECT * FROM loan")
    LiveData<List<Loan>> loadAllLoans();

    @Query("SELECT * FROM loan WHERE id==:id")
    LiveData<Loan> loadLoan(int id);

    @Query("SELECT COUNT(*) from loan")
    int getLoansLength();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertLoan(Loan l);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAllLoan(List<Loan> l);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateLoan(Loan loan);

    @Query("DELETE FROM loan WHERE id==:id")
    int deleteLoanById(long id);

}
