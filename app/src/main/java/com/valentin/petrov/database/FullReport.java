package com.valentin.petrov.database;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.ColumnInfo;
import androidx.room.DatabaseView;
import androidx.room.Ignore;

@DatabaseView(value = "SELECT * from report INNER JOIN loan ON report.loan_id = loan.id",
        viewName = "full_report")
public class FullReport implements Parcelable {
    @ColumnInfo(name = "id")
    public long id;

    @ColumnInfo(name = "report_name")
    public String reportName;

    @ColumnInfo(name = "address")
    public String address;

    @ColumnInfo(name = "city")
    public String city;

    @ColumnInfo(name = "zip")
    public String zip;

    @ColumnInfo(name = "image")
    public String imageURI;

    @ColumnInfo(name = "misc_text")
    public String miscText;

    @ColumnInfo(name = "purchase_price")
    public long purchasePrice;

    @ColumnInfo(name = "after_repair_value")
    public long afterRepairValue;

    @ColumnInfo(name = "repair_costs")
    public long repairCosts;

    @ColumnInfo(name = "closing_costs")
    public long closingCosts;

    @ColumnInfo(name = "cash_purchase")
    public boolean cashPurchase;

    @ColumnInfo(name = "loan_id")
    public Long loanId;

    @ColumnInfo(name = "cap_rate")
    public double capRate;

    @ColumnInfo(name = "amortization_rate")
    public int amortizationRate;

    @ColumnInfo(name = "rent")
    public int rent;

    @ColumnInfo(name = "other_income")
    public int otherIncome;

    @ColumnInfo(name = "electricity")
    public int electricity;

    @ColumnInfo(name = "sewer")
    public int sewer;

    @ColumnInfo(name = "water")
    public int water;

    @ColumnInfo(name = "garbage")
    public int garbage;

    @ColumnInfo(name = "hoa")
    public int hoa;

    @ColumnInfo(name = "insurance")
    public int insurance;

    @ColumnInfo(name = "property_tax")
    public int propertyTax;

    @ColumnInfo(name = "other_expenses")
    public int otherExpenses;

    @ColumnInfo(name = "vacancy")
    public double vacancy;

    @ColumnInfo(name = "maintenance")
    public double maintenance;

    @ColumnInfo(name = "cap_ex")
    public double capEX;

    @ColumnInfo(name = "management")
    public double management;

    @ColumnInfo(name = "income_inflation")
    public double incomeInflation;

    @ColumnInfo(name = "property_value_inflation")
    public double propertyValueInflation;

    @ColumnInfo(name = "expenses_inflation")
    public double expensesInflation;

    @ColumnInfo(name = "interest_only")
    public boolean interestOnly;

    @ColumnInfo(name = "loan_amount")
    public long loanAmount;

    @ColumnInfo(name = "lender_charges")
    public int lenderCharges;

    @ColumnInfo(name = "point_charges")
    public int pointCharges;

    @ColumnInfo(name = "interest_rate")
    public double interestRate;

    @ColumnInfo(name = "into_loan")
    public boolean intoLoanR;

    public FullReport(){}

    public Report convertToReport() {
        return new Report(reportName, address, city, zip, imageURI, miscText,
                purchasePrice, afterRepairValue, repairCosts, closingCosts,
                cashPurchase, loanId, capRate, rent, otherIncome, electricity,
                sewer, water, garbage, hoa, insurance, propertyTax,
                otherExpenses, vacancy, maintenance, capEX, management,
                incomeInflation, propertyValueInflation, expensesInflation);
    }

    public Loan convertToLoan() {
        return new Loan(id, interestOnly, loanAmount, lenderCharges,
                pointCharges, interestRate, intoLoanR, amortizationRate);
    }

    @Ignore
    public FullReport(String reportName, String address,
                      String city, String zip, String imageURI,
                      String miscText, long purchasePrice, long afterRepairValue,
                      int repairCosts, int closingCosts, boolean cashPurchase,
                      double capRate, int amortizationRate, int rent,
                      int otherIncome, int electricity, int sewer, int water,
                      int garbage, int hoa, int insurance, int propertyTax,
                      int otherExpenses, double vacancy, double maintenance,
                      double capEX, double management, double incomeInflation,
                      double propertyValueInflation, double expensesInflation,
                      boolean interestOnly, long loanAmount, int lenderCharges,
                      int pointCharges, double interestRate, boolean intoLoanR) {
        this.reportName = reportName;
        this.address = address;
        this.city = city;
        this.zip = zip;
        this.imageURI = imageURI;
        this.miscText = miscText;
        this.purchasePrice = purchasePrice;
        this.afterRepairValue = afterRepairValue;
        this.repairCosts = repairCosts;
        this.closingCosts = closingCosts;
        this.cashPurchase = cashPurchase;
        this.capRate = capRate;
        this.amortizationRate = amortizationRate;
        this.rent = rent;
        this.otherIncome = otherIncome;
        this.electricity = electricity;
        this.sewer = sewer;
        this.water = water;
        this.garbage = garbage;
        this.hoa = hoa;
        this.insurance = insurance;
        this.propertyTax = propertyTax;
        this.otherExpenses = otherExpenses;
        this.vacancy = vacancy;
        this.maintenance = maintenance;
        this.capEX = capEX;
        this.management = management;
        this.incomeInflation = incomeInflation;
        this.propertyValueInflation = propertyValueInflation;
        this.expensesInflation = expensesInflation;
        this.interestOnly = interestOnly;
        this.loanAmount = loanAmount;
        this.lenderCharges = lenderCharges;
        this.pointCharges = pointCharges;
        this.interestRate = interestRate;
        this.intoLoanR = intoLoanR;
    }

    protected FullReport(Parcel in) {
        this.reportName = in.readString();
        this.address = in.readString();
        this.city = in.readString();
        this.zip = in.readString();
        this.imageURI=in.readString();
        this.miscText = in.readString();
        this.purchasePrice = in.readInt();
        this.afterRepairValue = in.readInt();
        this.repairCosts = in.readInt();
        this.closingCosts = in.readInt();
        this.cashPurchase = in.readByte() != 0; //ffs
        this.capRate = in.readInt();
        this.amortizationRate = in.readInt();
        this.rent = in.readInt();
        this.otherIncome = in.readInt();
        this.electricity = in.readInt();
        this.sewer = in.readInt();
        this.water = in.readInt();
        this.garbage = in.readInt();
        this.hoa = in.readInt();
        this.insurance = in.readInt();
        this.propertyTax = in.readInt();
        this.otherExpenses = in.readInt();
        this.vacancy = in.readInt();
        this.maintenance = in.readInt();
        this.capEX = in.readInt();
        this.management = in.readInt();
        this.incomeInflation = in.readInt();
        this.propertyValueInflation = in.readInt();
        this.expensesInflation = in.readInt();
        this.interestOnly = in.readByte() != 0;
        this.loanAmount = in.readInt();
        this.lenderCharges = in.readInt();
        this.pointCharges = in.readInt();
        this.interestRate = in.readInt();
        this.intoLoanR = in.readByte() != 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.reportName);
        dest.writeString(this.address);
        dest.writeString(this.city);
        dest.writeString(this.zip);
        dest.writeString(this.imageURI);
        dest.writeString(this.miscText);
        dest.writeLong(this.purchasePrice);
        dest.writeLong(this.afterRepairValue);
        dest.writeLong(this.repairCosts);
        dest.writeLong(this.closingCosts);
        dest.writeByte((byte) (this.cashPurchase ? 1 : 0));//ffs
        dest.writeDouble(this.capRate);
        dest.writeInt(this.amortizationRate);
        dest.writeInt(this.rent);
        dest.writeInt(this.otherIncome);
        dest.writeInt(this.electricity);
        dest.writeInt(this.sewer);
        dest.writeInt(this.water);
        dest.writeInt(this.garbage);
        dest.writeInt(this.hoa);
        dest.writeInt(this.insurance);
        dest.writeInt(this.propertyTax);
        dest.writeInt(this.otherExpenses);
        dest.writeDouble(this.vacancy);
        dest.writeDouble(this.maintenance);
        dest.writeDouble(this.capEX);
        dest.writeDouble(this.management);
        dest.writeDouble(this.incomeInflation);
        dest.writeDouble(this.propertyValueInflation);
        dest.writeDouble(this.expensesInflation);
        dest.writeByte((byte) (this.interestOnly ? 1 : 0));
        dest.writeDouble(this.loanAmount);
        dest.writeInt(this.lenderCharges);
        dest.writeInt(this.pointCharges);
        dest.writeDouble(this.interestRate);
        dest.writeByte((byte) (this.intoLoanR ? 1 : 0));
    }

    @Override
    public int describeContents() {
        return this.hashCode();
    }

    public static final Creator<FullReport> CREATOR = new Creator<FullReport>() {
        @Override
        public FullReport createFromParcel(Parcel in) {
            return new FullReport(in);
        }

        @Override
        public FullReport[] newArray(int size) {
            return new FullReport[size];
        }
    };
}

