package com.valentin.petrov.database;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import static androidx.room.ForeignKey.CASCADE;

@Entity(indices = {@Index(value = {"id"}, unique = true),@Index(value = {"id","loan_id"},unique = true),
        @Index(value = {"loan_id"},unique = true)},
        foreignKeys = {@ForeignKey(onDelete = CASCADE,entity = Loan.class,
                parentColumns = "id", childColumns = "loan_id")})
public class Report {
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    public long id;

    @ColumnInfo(name = "report_name")
    public String reportName;

    @ColumnInfo(name = "address")
    public String address;

    @ColumnInfo(name = "city")
    public String city;

    @ColumnInfo(name = "zip")
    public String zip;

    @ColumnInfo(name = "image")
    public String imageURI;

    @ColumnInfo(name = "misc_text")
    public String miscText;

    @ColumnInfo(name = "purchase_price")
    public long purchasePrice;

    @ColumnInfo(name = "after_repair_value")
    public long afterRepairValue;

    @ColumnInfo(name = "repair_costs")
    public long repairCosts;

    @ColumnInfo(name = "closing_costs")
    public long closingCosts;

    @ColumnInfo(name = "cash_purchase")
    public boolean cashPurchase;

    @ColumnInfo(name = "loan_id")
    public Long loanId;

    @ColumnInfo(name = "cap_rate")
    public double capRate;

    @ColumnInfo(name = "rent")
    public int rent;

    @ColumnInfo(name = "other_income")
    public int otherIncome;

    @ColumnInfo(name = "electricity")
    public int electricity;

    @ColumnInfo(name = "sewer")
    public int sewer;

    @ColumnInfo(name = "water")
    public int water;

    @ColumnInfo(name = "garbage")
    public int garbage;

    @ColumnInfo(name = "hoa")
    public int hoa;

    @ColumnInfo(name = "insurance")
    public int insurance;

    @ColumnInfo(name = "property_tax")
    public int propertyTax;

    @ColumnInfo(name = "other_expenses")
    public int otherExpenses;

    @ColumnInfo(name = "vacancy")
    public double vacancy;

    @ColumnInfo(name = "maintenance")
    public double maintenance;

    @ColumnInfo(name = "cap_ex")
    public double capEX;

    @ColumnInfo(name = "management")
    public double management;

    @ColumnInfo(name = "income_inflation")
    public double incomeInflation;

    @ColumnInfo(name = "property_value_inflation")
    public double propertyValueInflation;

    @ColumnInfo(name = "expenses_inflation")
    public double expensesInflation;

    public Report (){}

    @Ignore
    public Report(String reportName, String address, String city,
                  String zip, String imageURI, String miscText,
                  long purchasePrice, long afterRepairValue, long repairCosts,
                  long closingCosts, boolean cashPurchase, Long loanId,
                  double capRate, int rent, int otherIncome,
                  int electricity, int sewer, int water, int garbage, int hoa,
                  int insurance, int propertyTax, int otherExpenses,
                  double vacancy, double maintenance, double capEX, double management,
                  double incomeInflation, double propertyValueInflation,
                  double expensesInflation) {
        this.reportName = reportName;
        this.address = address;
        this.city = city;
        this.zip = zip;
        this.imageURI = imageURI;
        this.miscText = miscText;
        this.purchasePrice = purchasePrice;
        this.afterRepairValue = afterRepairValue;
        this.repairCosts = repairCosts;
        this.closingCosts = closingCosts;
        this.cashPurchase = cashPurchase;
        this.loanId = loanId;
        this.capRate = capRate;
        this.rent = rent;
        this.otherIncome = otherIncome;
        this.electricity = electricity;
        this.sewer = sewer;
        this.water = water;
        this.garbage = garbage;
        this.hoa = hoa;
        this.insurance = insurance;
        this.propertyTax = propertyTax;
        this.otherExpenses = otherExpenses;
        this.vacancy = vacancy;
        this.maintenance = maintenance;
        this.capEX = capEX;
        this.management = management;
        this.incomeInflation = incomeInflation;
        this.propertyValueInflation = propertyValueInflation;
        this.expensesInflation = expensesInflation;
    }
}
