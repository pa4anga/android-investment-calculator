package com.valentin.petrov.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

@Dao
public interface ReportDAO {

    @Query("SELECT * FROM report")
    LiveData<List<Report>> loadAllReports();

    @Query("SELECT id,report_name,image FROM report")
    LiveData<List<MainMenuReport>> loadAllDisplayReports();

    @Query("SELECT * FROM report WHERE id==:id")
    LiveData<Report> loadReportById(int id);

    @Query("SELECT * FROM report")
    List<Report> loadAllReportsSync();

    @Query("SELECT * FROM report WHERE id==:id")
    Report loadReportByIdSync(long id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertReport(Report r);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long[] insertAllReports(List<Report> r);

    @Query("DELETE FROM report WHERE id==:id")
    int deleteReportById(long id);

    @Query("SELECT COUNT(*) FROM report WHERE image==:imageURI ")
    int getNumberOfReportsUsingImage(String imageURI);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int updateReport( Report r);

}
