package com.valentin.petrov.database;

import androidx.room.ColumnInfo;

public class MainMenuReport {
    @ColumnInfo(name="id")
    public long id;
    @ColumnInfo(name="report_name")
    public String reportName;
    @ColumnInfo(name = "image")
    public String imageURI;
}
