package com.valentin.petrov.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import java.util.List;

@Dao
public interface FullReportDAO {
    @Query("SELECT * from report LEFT JOIN loan ON report.loan_id=loan.id")
    LiveData<List<FullReport>> loadAllFullReports();

    @Query("SELECT * from report LEFT JOIN loan ON report.loan_id=loan.id WHERE report.id=:id")
    LiveData<FullReport> loadFullReportById(long id);

    @Query("SELECT * from report LEFT JOIN loan ON report.loan_id=loan.id WHERE report.id=:id")
    FullReport loadFullReportByIdSync(long id);

    @Query("SELECT * from report LEFT JOIN loan ON report.loan_id=loan.id")
    FullReport loadAllReportsSync();

    @Query("SELECT COUNT(*) FROM report LEFT JOIN loan ON report.loan_id=loan.id WHERE report.id=:id")
    int checkIfReportExists(long id);




}
