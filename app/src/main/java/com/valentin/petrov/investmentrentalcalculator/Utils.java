package com.valentin.petrov.investmentrentalcalculator;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.TextView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.exifinterface.media.ExifInterface;

import com.valentin.petrov.database.FullReport;
import com.valentin.petrov.database.Report;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Utils {
    public static Bitmap convertDrawableToBitmap(Drawable drawable) {
        Bitmap bitmap = null;

        if (drawable instanceof BitmapDrawable) {
            BitmapDrawable bitmapDrawable = (BitmapDrawable) drawable;
            if (bitmapDrawable.getBitmap() != null) {
                return bitmapDrawable.getBitmap();
            }
        }

        if (drawable.getIntrinsicWidth() <= 0 || drawable.getIntrinsicHeight() <= 0) {
            bitmap = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888); // Single color bitmap will be created of 1x1 pixel
        } else {
            bitmap = Bitmap.createBitmap(drawable.getIntrinsicWidth(),
                    drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        }

        Canvas canvas = new Canvas(bitmap);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bitmap;
    }

    public static byte[] convertBitmapToBytes(Bitmap bitmap) {
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 0, stream);
        return stream.toByteArray();
    }

    private static RoundedBitmapDrawable getRoundedCornerDrawableFromURI(Context context, Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media
                .getBitmap(context.getContentResolver(),uri);
        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory
                .create(context.getResources(),bitmap);
        drawable.setCornerRadius(30);
        return drawable;
    }

    private static RoundedBitmapDrawable getRoundedCornerDrawableFromOutsideURI(Context context, Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media
                .getBitmap(context.getContentResolver(),uri);
        bitmap = compressBitmap(bitmap,
                context.getResources().getInteger(R.integer.max_image_size));
        int rotationAngle = getRotationNeeded(uri,context);
        if(rotationAngle!=0){
            Matrix matrix = new Matrix();
            matrix.postRotate(rotationAngle);
            bitmap = Bitmap.createBitmap(bitmap,0,0,bitmap.getWidth(),
                    bitmap.getHeight(),matrix,true);
        }

        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory
                .create(context.getResources(),bitmap);
        drawable.setCornerRadius(30);
        return drawable;
    }

    public static Bitmap convertBytesToBitmap(byte[] image) {
        return BitmapFactory.decodeByteArray(image, 0, image.length);
    }

    /**@return rotation needed in degrees**/
    private static int getRotationNeeded(Uri imageUri,Context context){
        context.getContentResolver().notifyChange(imageUri, null);
        int rotate = 0;
        try (InputStream in = context.getContentResolver().openInputStream(imageUri)) {
            ExifInterface exif = new ExifInterface(in);
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);

            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Log.d("RotateImage", "Exif orientation: " + orientation);
            Log.d("RotateImage", "Rotate value: " + rotate);
        } catch (IOException e) {
            Log.e("IO",e.getMessage());
            Log.e("IO", Arrays.toString(e.getStackTrace()));
            return 0;
        }

        return rotate;
    }


    private static Bitmap compressBitmap(Bitmap bitmap, int max_image_size) {//TODO look at replacing this with something more reliable
        Bitmap compressed;
        int targetWidth = (int) (bitmap.getWidth() * max_image_size
                / (double) bitmap.getHeight()); // casts to avoid truncating

        compressed =
                Bitmap.createScaledBitmap(bitmap, targetWidth, max_image_size,
                        true);
        return compressed;
    }

    public static List<Report> getTestReports() {
        FullReport r;
        List<Report> testEntries = new LinkedList<>();
        for (int i = 1; i < 20; i++) {
            r = new FullReport("test" + i, "a", "a", "a", "", "", 0, 0, 0, 0, false, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, false, 0, 0, 0, 0, false);
            testEntries.add(r.convertToReport());
        }
        return testEntries;
    }

    public static Uri resolveUriFromString(String rawUri){
        Uri resolvedUri=null;
        try{
            resolvedUri=Uri.parse(rawUri);
        } catch (Exception e){
            Log.e("URI",e.getMessage());
            Log.e("URI", Arrays.toString(e.getStackTrace()));
        }
        return resolvedUri;
    }

    public static Drawable getDrawableFromOutsideURI(Context context, Uri uri) {
        if(uri==null)return context.getDrawable(R.drawable.ic_24px);

        InputStream is;
        try {
            is = context.getContentResolver().openInputStream(uri);
            RoundedBitmapDrawable drawable;
            drawable = getRoundedCornerDrawableFromOutsideURI
                    (context.getApplicationContext(),uri);
            drawable.setCornerRadius(16);
            return drawable;

        } catch (Exception e) {
            Log.e("URI failed to resolve",
                    (e.getMessage()) + "\n" + Arrays.toString(e.getStackTrace()));
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return context.getDrawable(R.drawable.ic_24px);

    }

    public static Drawable getDrawableFromURI(Context context, Uri uri) {
        if(uri==null)return context.getDrawable(R.drawable.ic_24px);

        InputStream is;
        try {
            is = context.getContentResolver().openInputStream(uri);
            RoundedBitmapDrawable drawable;
            drawable = getRoundedCornerDrawableFromURI
                    (context.getApplicationContext(),uri);
            drawable.setCornerRadius(16);
            return drawable;

        } catch (Exception e) {
            Log.e("URI failed to resolve",
                    (e.getMessage()) + "\n" + Arrays.toString(e.getStackTrace()));
            System.err.println(e.getMessage());
            e.printStackTrace();
        }

        return context.getDrawable(R.drawable.ic_24px);
        //TODO IMPROVE PLACEHOLDER ICON
    }

    public static String formatToMoney(double val){//Include Money when implemented here
        return new BigDecimal(val).setScale(2,BigDecimal.ROUND_HALF_UP).toString();
    }

    public static String formatToPercent(double val){
        return formatToMoney(val)+"%";
    }

    public static void displayTextInLabel(int message, int color, TextView target) {
        target.setText(message);
        target.setTextColor(color);
    }

    public static void displayTextInLabel(String message, int color, TextView target) {
        target.setText(message);
        target.setTextColor(color);
    }
}
