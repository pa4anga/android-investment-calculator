package com.valentin.petrov.mainmenu;

import android.graphics.drawable.Drawable;

class RenderedReport {
    Drawable image;
    long id;
    String reportName;
    String imageURI;

    RenderedReport(long id, String reportName, Drawable image, String imageURI){
        this.id=id;
        this.reportName=reportName;
        this.image=image;
        this.imageURI=imageURI;
    }
}
