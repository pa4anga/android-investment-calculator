package com.valentin.petrov.mainmenu;

import com.valentin.petrov.database.MainMenuReport;

class MainMenuReportComparator {
    /**Compares reports on id imageURI and name only
     * @return 1 if not the same id, 2 if same id but different contents, 0 identical**/
    int compare(MainMenuReport o1, RenderedReport o2) {
        if (o1.id!=o2.id)return 2;
        if (!o1.reportName.equals(o2.reportName))return 1;

        if(o1.imageURI==null&&o2.imageURI==null){
            return 0;//both images are null
        }
        else {
            if(o1.imageURI!=null&&o2.imageURI==null) {
                return 1;
            }
            else {
                if(o1.imageURI==null&&o2.imageURI!=null) {
                    return 1;
                }
                else {

                    if (!o1.imageURI.equals(o2.imageURI)){
                        return 1;
                    }
                }
            }
        }

        return 0;

    }
}
