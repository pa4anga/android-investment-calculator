package com.valentin.petrov.mainmenu;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnCreateContextMenuListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.valentin.petrov.database.MainMenuReport;
import com.valentin.petrov.investmentrentalcalculator.R;

import java.util.List;

public class ReportsRecyclerAdapter extends RecyclerView.Adapter {

    private Context context;
    private List<RenderedReport> reports;
    private View.OnClickListener cardOnClickListener;
    private OnCreateContextMenuListener contextMenuListener;

    ReportsRecyclerAdapter(@NonNull Context context, @NonNull List<RenderedReport> reports,
                           View.OnClickListener cardOnClickListener,
                           View.OnCreateContextMenuListener contextMenuListener) {
        this.reports = reports;
        this.context = context;
        this.cardOnClickListener = cardOnClickListener;
        this.contextMenuListener = contextMenuListener;
    }

    public void removeDeletedReports(List<MainMenuReport> reports) {
        boolean inNew;
        for(int i=0;i<this.getReports().size();i++){
            inNew=false;

            for(MainMenuReport r:reports){
                if(r.id==this.getReports().get(i).id){
                    inNew = true;
                    break;
                }
            }

            if(!inNew){
                removeData(i);
                i--;
            }
        }
    }

    private void removeData(int position) {
        this.reports.remove(position);
        this.notifyItemRemoved(position);
        this.notifyItemRangeRemoved(position,getItemCount());
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final ImageView imageView;
        TextView textView;
        CardView cardView;

        ViewHolder(ConstraintLayout v) {
            super(v);
            cardView = (CardView) v.findViewById(R.id.report_card);
            textView = (TextView) v.findViewById(R.id.report_card_text);
            imageView = (ImageView) v.findViewById(R.id.report_card_image);
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ConstraintLayout v = (ConstraintLayout) LayoutInflater.from(context)
                .inflate(R.layout.report_recycle_card, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {

        ((ViewHolder) holder)
                .textView.setText(reports.get(position).reportName);

        ((ViewHolder) holder).cardView.setTag(reports.get(position).id);
        ((ViewHolder) holder).cardView.setOnClickListener(cardOnClickListener);
        ((ViewHolder) holder).cardView
                .setOnCreateContextMenuListener(contextMenuListener);

        ((ViewHolder) holder).imageView.setImageDrawable(reports.get(position).image);
    }

    void updateList(List<RenderedReport> r) {//TODO IMPLEMENT DIFF CHECKING
        reports.addAll(r);
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return reports.size();
    }

    List<RenderedReport> getReports() {
        return reports;
    }
}
