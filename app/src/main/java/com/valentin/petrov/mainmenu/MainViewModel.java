package com.valentin.petrov.mainmenu;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import com.valentin.petrov.database.MainMenuReport;
import com.valentin.petrov.database.ReportsRepository;

import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private LiveData<List<MainMenuReport>> reports;

    public MainViewModel(@NonNull Application application) {
        super(application);
        ReportsRepository repository = ReportsRepository.getInstance(application);
        this.reports= repository.getDisplayReports();
    }

    LiveData<List<MainMenuReport>> getReports() {
        return reports;
    }
}
