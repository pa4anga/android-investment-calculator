package com.valentin.petrov.mainmenu;

import android.app.Activity;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.valentin.petrov.database.MainMenuReport;
import com.valentin.petrov.database.ReportsRepository;
import com.valentin.petrov.dataentry.DataEntryActivity;
import com.valentin.petrov.displayreport.DisplayReportActivity;
import com.valentin.petrov.investmentrentalcalculator.R;
import com.valentin.petrov.investmentrentalcalculator.Utils;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static com.valentin.petrov.dataentry.DataEntryActivity.EDIT;
import static com.valentin.petrov.dataentry.DataEntryActivity.ID;
import static com.valentin.petrov.dataentry.DataEntryActivity.MODE;

public class MainActivity extends AppCompatActivity {


    private static final int FULL_REPORT_CODE = 3082;
    private MainViewModel model;
    private View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            long id = (Long) view.getTag();
            startNewDisplayReportActivity(id);
        }
    };
    private View.OnCreateContextMenuListener contextMenuListener = new View.OnCreateContextMenuListener(){
        @Override
        public void onCreateContextMenu(ContextMenu menu, final View v,ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(R.string.Edit).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    long id = (long) v.getTag();
                    startNewDataEditingActivity(id);
                    return true;
                }
            });
            menu.add(R.string.Delete).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
                @Override
                public boolean onMenuItemClick(MenuItem item) {
                    long id = (long) v.getTag();
                    ReportsRepository.getInstance(getApplication())
                            .deleteReportById(id);
                    return true;
                }
            });
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.model= ViewModelProviders.of(this).get(MainViewModel.class);
        setContentView(R.layout.activity_main);

        final RecyclerView reportsView = findViewById(R.id.reportsView);
        reportsView.setLayoutManager(new GridLayoutManager(getApplicationContext(), 2));

        reportsView.setAdapter
                (new ReportsRecyclerAdapter(getApplicationContext(),
                        new ArrayList<RenderedReport>(),onItemClickListener,
                        contextMenuListener));

        final Observer<List<MainMenuReport>> reportObserver = new Observer<List<MainMenuReport>>() {
            @Override
            public void onChanged(List<MainMenuReport> reports) {
                ReportsRecyclerAdapter adapter=
                        (ReportsRecyclerAdapter)reportsView.getAdapter();

                if(adapter==null){
                    adapter=new ReportsRecyclerAdapter
                            (getApplicationContext(),
                                    new LinkedList<RenderedReport>(),
                                    onItemClickListener,contextMenuListener);

                    reportsView.setAdapter(adapter);
                }

                if(reports==null){
                    reports=new LinkedList<>();
                }

                adapter.removeDeletedReports(reports);
                MainMenuReportComparator comparator = new MainMenuReportComparator();
                if(adapter.getReports()!=null){
                    for(int j=0;j<adapter.getReports().size();j++){
                        for(int i=0;i<reports.size();i++){
                            int compareResult =comparator.compare(reports.get(i),
                                    adapter.getReports().get(j));

                            switch (compareResult){
                                case 0:
                                    reports.remove(i);
                                    i--;
                                    break;

                                case 1:
                                    adapter.getReports().remove(j);
                                    break;
                            }
                        }
                    }
                }

                List<RenderedReport> renderedReports= new LinkedList<>();

                for(MainMenuReport r:reports){

                    Uri resolvedUri= Utils.resolveUriFromString(r.imageURI);
                    Drawable d= Utils.getDrawableFromURI
                            (getApplicationContext(),resolvedUri);

                    renderedReports
                            .add(new RenderedReport(r.id,r.reportName,d,r.imageURI));
                }

                ((ReportsRecyclerAdapter)reportsView.getAdapter())
                        .updateList(renderedReports);
            }
        };

        model.getReports().observe(this,reportObserver);
    }

    private void startNewDisplayReportActivity(long id){
        Intent intent = new Intent(this, DisplayReportActivity.class);
        intent.putExtra(DisplayReportActivity.ID,id);
        startActivity(intent);
    }

    private void startNewDataEditingActivity(long id){
        Intent intent = new Intent(this, DataEntryActivity.class);
        intent.putExtra(MODE,EDIT);
        intent.putExtra(ID,id);
        startActivityForResult(intent, FULL_REPORT_CODE);
    }

    public void startNewDataEntryActivity(View view) {
        Intent intent = new Intent(this, DataEntryActivity.class);
        startActivityForResult(intent, FULL_REPORT_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == FULL_REPORT_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                long reportId = data.getLongExtra("ReportId",0);

                startNewDisplayReportActivity(reportId);
            }
        }
    }
}
