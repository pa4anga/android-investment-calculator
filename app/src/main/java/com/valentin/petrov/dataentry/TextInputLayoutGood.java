package com.valentin.petrov.dataentry;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;

import com.google.android.material.textfield.TextInputLayout;

public class TextInputLayoutGood extends TextInputLayout {

    public TextInputLayoutGood(Context context) {
        super(context);
    }

    public TextInputLayoutGood(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TextInputLayoutGood(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void style(int color, int textStyleId){
        setHintTextAppearance(textStyleId);
        setUnderlineColor(color);
        if(this.getEditText()!=null){
            this.getEditText().setHintTextColor(color);
        }
    }

    private void setUnderlineColor(int color) {
        if (getEditText() != null) {
            getEditText().getBackground()
                    .setColorFilter(color, PorterDuff.Mode.SRC_ATOP);
        }
    }

}
