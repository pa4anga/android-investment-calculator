package com.valentin.petrov.dataentry;

import com.valentin.petrov.database.FullReport;

public interface ReportEditor {
    void onEditReportRetrieved(FullReport report);
}
