package com.valentin.petrov.dataentry;

import android.app.Application;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

import com.valentin.petrov.database.ReportsRepository;
import com.valentin.petrov.investmentrentalcalculator.Utils;

public class DataEntryViewModel extends AndroidViewModel {
    ReportsRepository repo;

    private Drawable imageButton;

    private String tempURI;

    public DataEntryViewModel(@NonNull Application application) {
        super(application);
        this.repo = ReportsRepository.getInstance(application);
    }

    boolean isSynced(){
        return imageButton !=null;
    }

    String exportTempImageURI(){
        return tempURI;
    }

    void restoreFromSaveState(Context context, String tempURI){
        this.tempURI=tempURI;
        if(tempURI!=null) {
            this.imageButton = Utils.getDrawableFromURI(context, Uri.parse(this.tempURI));
        }
    }

    Drawable getImageButton() {
        return imageButton;
    }

    void setImageButton(Drawable imageButton) {
        this.imageButton = imageButton;
    }

    void setTempURI(String tempImageURI) {
        this.tempURI=tempImageURI;
    }
}
