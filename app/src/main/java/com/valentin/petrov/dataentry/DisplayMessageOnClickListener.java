package com.valentin.petrov.dataentry;

import android.view.View;

import androidx.fragment.app.FragmentManager;

public class DisplayMessageOnClickListener implements View.OnClickListener {

    private String title;
    private String message;
    private FragmentManager fragmentManager;

    public DisplayMessageOnClickListener(String title, String message, FragmentManager fragmentManager) {
        this.title = title;
        this.message = message;
        this.fragmentManager=fragmentManager;
    }

    @Override
    public void onClick(View v) {
         MessageFragment.displayMessage(title,message).show(fragmentManager,null);
    }
}
