package com.valentin.petrov.dataentry;

import android.os.Parcel;
import android.os.Parcelable;

public class ModelState implements Parcelable {
    public String reportName;
    public String address;
    public String city;
    public String zip;
    public String miscText;

    public String tempURI;

    public String purchasePrice;
    public String afterRepairValue;
    public String repairCosts;
    public String closingCosts;
    public boolean interestOnly;
    public boolean cashPurchase;
    public String loanAmount;
    public String lenderCharges;
    public String pointCharges;
    public String interestRate;
    public boolean intoLoanR;
    public boolean directlyR;

    public String capRate;
    public String amortizationRate;
    public String rent;
    public String otherIncome;
    public String electricity;
    public String sewer;
    public String water;
    public String garbage;
    public String HOA;
    public String insurance;

    public String propertyTax;
    public String otherExpenses;
    public String vacancy;
    public String maintenance;
    public String capEx;
    public String management;
    public String incomeInflation;
    public String propertyValueInflation;
    public String expensesInflation;

    public ModelState(String reportName, String address, String city, String zip,
                      String miscText, String tempURI, String purchasePrice,
                      String afterRepairValue, String repairCosts, String closingCosts,
                      boolean interestOnly, boolean cashPurchase, String loanAmount,
                      String lenderCharges, String pointCharges, String interestRate,
                      boolean intoLoanR, boolean directlyR, String capRate,
                      String amortizationRate, String rent, String otherIncome,
                      String electricity, String sewer, String water, String garbage,
                      String HOA, String insurance, String propertyTax,
                      String otherExpenses, String vacancy, String maintenance,
                      String capEx, String management, String incomeInflation,
                      String propertyValueInflation, String expensesInflation) {
        this.reportName = reportName;
        this.address = address;
        this.city = city;
        this.zip = zip;
        this.miscText = miscText;
        this.tempURI = tempURI;
        this.purchasePrice = purchasePrice;
        this.afterRepairValue = afterRepairValue;
        this.repairCosts = repairCosts;
        this.closingCosts = closingCosts;
        this.interestOnly = interestOnly;
        this.cashPurchase = cashPurchase;
        this.loanAmount = loanAmount;
        this.lenderCharges = lenderCharges;
        this.pointCharges = pointCharges;
        this.interestRate = interestRate;
        this.intoLoanR = intoLoanR;
        this.directlyR = directlyR;
        this.capRate = capRate;
        this.amortizationRate = amortizationRate;
        this.rent = rent;
        this.otherIncome = otherIncome;
        this.electricity = electricity;
        this.sewer = sewer;
        this.water = water;
        this.garbage = garbage;
        this.HOA = HOA;
        this.insurance = insurance;
        this.propertyTax = propertyTax;
        this.otherExpenses = otherExpenses;
        this.vacancy = vacancy;
        this.maintenance = maintenance;
        this.capEx = capEx;
        this.management = management;
        this.incomeInflation = incomeInflation;
        this.propertyValueInflation = propertyValueInflation;
        this.expensesInflation = expensesInflation;
    }

    protected ModelState(Parcel in) {
        reportName = in.readString();
        address = in.readString();
        city = in.readString();
        zip = in.readString();
        miscText = in.readString();
        tempURI = in.readString();
        purchasePrice = in.readString();
        afterRepairValue = in.readString();
        repairCosts = in.readString();
        closingCosts = in.readString();
        interestOnly = in.readByte() != 0;
        cashPurchase = in.readByte() != 0;
        loanAmount = in.readString();
        lenderCharges = in.readString();
        pointCharges = in.readString();
        interestRate = in.readString();
        intoLoanR = in.readByte() != 0;
        directlyR = in.readByte() != 0;
        capRate = in.readString();
        amortizationRate = in.readString();
        rent = in.readString();
        otherIncome = in.readString();
        electricity = in.readString();
        sewer = in.readString();
        water = in.readString();
        garbage = in.readString();
        HOA = in.readString();
        insurance = in.readString();
        propertyTax = in.readString();
        otherExpenses = in.readString();
        vacancy = in.readString();
        maintenance = in.readString();
        capEx = in.readString();
        management = in.readString();
        incomeInflation = in.readString();
        propertyValueInflation = in.readString();
        expensesInflation = in.readString();
    }

    public static final Creator<ModelState> CREATOR = new Creator<ModelState>() {
        @Override
        public ModelState createFromParcel(Parcel in) {
            return new ModelState(in);
        }

        @Override
        public ModelState[] newArray(int size) {
            return new ModelState[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(reportName);
        dest.writeString(address);
        dest.writeString(city);
        dest.writeString(zip);
        dest.writeString(miscText);
        dest.writeString(tempURI);
        dest.writeString(purchasePrice);
        dest.writeString(afterRepairValue);
        dest.writeString(repairCosts);
        dest.writeString(closingCosts);
        dest.writeByte((byte) (interestOnly ? 1 : 0));
        dest.writeByte((byte) (cashPurchase ? 1 : 0));
        dest.writeString(loanAmount);
        dest.writeString(lenderCharges);
        dest.writeString(pointCharges);
        dest.writeString(interestRate);
        dest.writeByte((byte) (intoLoanR ? 1 : 0));
        dest.writeByte((byte) (directlyR ? 1 : 0));
        dest.writeString(capRate);
        dest.writeString(amortizationRate);
        dest.writeString(rent);
        dest.writeString(otherIncome);
        dest.writeString(electricity);
        dest.writeString(sewer);
        dest.writeString(water);
        dest.writeString(garbage);
        dest.writeString(HOA);
        dest.writeString(insurance);
        dest.writeString(propertyTax);
        dest.writeString(otherExpenses);
        dest.writeString(vacancy);
        dest.writeString(maintenance);
        dest.writeString(capEx);
        dest.writeString(management);
        dest.writeString(incomeInflation);
        dest.writeString(propertyValueInflation);
        dest.writeString(expensesInflation);
    }
}
