package com.valentin.petrov.dataentry;

public interface ReportInserter {
    void onReportInserted(long id);
}
