package com.valentin.petrov.dataentry;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.lifecycle.ViewModelProviders;

import com.google.android.material.textfield.TextInputLayout;
import com.rfksystems.blake2b.Blake2b;
import com.rfksystems.blake2b.security.Blake2bProvider;
import com.valentin.petrov.database.FullReport;
import com.valentin.petrov.database.ReportsRepository;
import com.valentin.petrov.investmentrentalcalculator.R;
import com.valentin.petrov.investmentrentalcalculator.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.InvalidParameterException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Arrays;


public class DataEntryActivity extends AppCompatActivity implements ReportEditor, ReportInserter {

    public static final String TEMP_IMAGE_URI_KEY = "ViewModelSaveInstance";
    public static final String MODE = "Mode";
    public static final String ID = "id";
    public static final String SYNC = "sync";
    private static final int myFileSystemPermission = 2803;
    private DataEntryViewModel model;
    private int mode;
    public static final int EDIT=1;
    public static final int CREATE=0;
    private int synced;

    private static final int PICK_IMAGE = 33;
    private String tempImageURI;
    private static final String TEMP_FILE_NAME = "TEMP_IMAGE_FILE.bmp";

    ReportsRepository repo;

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        String tempImageURI = model.exportTempImageURI();
        outState.putString(TEMP_IMAGE_URI_KEY,tempImageURI);
        outState.putInt(SYNC,synced);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        synced=0;
        super.onCreate(savedInstanceState);
        this.mode=getIntent().getIntExtra(MODE,CREATE);

        setContentView(R.layout.activity_data_entry);

        attachHelpListeners();

        attachErrorMessageListeners();

        findViewById(R.id.ImageGetterButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getImageFromFileSystem();
            }
        });

        this.model = ViewModelProviders.of(this).get(DataEntryViewModel.class);

        this.repo = ReportsRepository.getInstance(getApplication());

        if (savedInstanceState != null) {
            this.synced = savedInstanceState.getInt(SYNC);
            String tempURI=savedInstanceState.getString(TEMP_IMAGE_URI_KEY);
            model.restoreFromSaveState(getApplicationContext(),tempURI);

            return;
        }

        if(mode==EDIT&&synced==0){//so it only syncs once

            long id = getIntent().getLongExtra(ID,-1);
            if(id == -1){
                throw new InvalidParameterException("No id provided for editing in intent");
            }
            repo.loadReportById(id,this);//triggers callback onEditReportReceived
        }

    }

    private void attachErrorMessageListeners() {
        attachPropertyInfoErrorMessageListeners();
        attachAcquisitionDetailsErrorMessageListeners();
        attachIncomeDetailsErrorMessageListeners();
        attachFixedExpensesErrorMessageListeners();
        attachVariableExpensesErrorMessageListeners();
        attachFutureAssumptionsErrorMessageListeners();
    }

    private void attachFutureAssumptionsErrorMessageListeners() {
        findViewById(R.id.IncomeInflation).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.IncomeInflation, R.id.income_inflation_container,
                            R.id.income_inflation_error)) {

                        isNotInValidRange(R.id.IncomeInflation,R.id.income_inflation_container,
                                R.id.income_inflation_error,0,100.);
                    }
                }
            }
        });

        findViewById(R.id.PropertyValueInflation).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.PropertyValueInflation, R.id.property_value_inflation_container,
                            R.id.property_value_inflation_error)) {

                        isNotInValidRange(R.id.PropertyValueInflation,R.id.property_value_inflation_container,
                                R.id.property_value_inflation_error,0,100.);
                    }
                }
            }
        });

        findViewById(R.id.ExpensesInflation).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.ExpensesInflation, R.id.expenses_inflation_container,
                            R.id.expenses_inflation_error)) {

                        isNotInValidRange(R.id.ExpensesInflation,R.id.expenses_inflation_container,
                                R.id.expenses_inflation_error,0,100.);
                    }
                }
            }
        });
    }

    private void attachVariableExpensesErrorMessageListeners() {
        findViewById(R.id.Vacancy).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.Vacancy, R.id.vacancy_container,
                            R.id.vacancy_error)) {

                        isNotInValidRange(R.id.Vacancy,R.id.vacancy_container,
                                R.id.vacancy_error,0,100.);
                    }
                }
            }
        });

        findViewById(R.id.Maintenance).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.Maintenance, R.id.maintenance_container,
                            R.id.maintenance_error)) {

                        isNotInValidRange(R.id.Maintenance,R.id.maintenance_container,
                                R.id.maintenance_error,0,100.);
                    }
                }
            }
        });

        findViewById(R.id.CapEx).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.CapEx, R.id.cap_ex_container,
                            R.id.cap_ex_error)) {

                        isNotInValidRange(R.id.Maintenance,R.id.cap_ex_container,
                                R.id.cap_ex_error,0,100.);
                    }
                }
            }
        });

        findViewById(R.id.ManagementFees).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.ManagementFees, R.id.management_fees_container,
                            R.id.management_fees_error)) {

                        isNotInValidRange(R.id.ManagementFees,R.id.management_fees_container,
                                R.id.management_fees_error,0,100.);
                    }
                }
            }
        });
    }

    private void attachFixedExpensesErrorMessageListeners() {

        findViewById(R.id.ElectricityExpenses).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.ElectricityExpenses, R.id.electricity_expenses_container,
                            R.id.electricity_expenses_error)) {

                        isNotInValidRange(R.id.ElectricityExpenses,R.id.electricity_expenses_container,
                                R.id.electricity_expenses_error,0,50000);
                    }
                }
            }
        });

        findViewById(R.id.SewerExpenses).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.SewerExpenses, R.id.sewer_expenses_container,
                            R.id.electricity_expenses_error)) {

                        isNotInValidRange(R.id.SewerExpenses,R.id.sewer_expenses_container,
                                R.id.sewer_expenses_error,0,20000);
                    }
                }
            }
        });

        findViewById(R.id.WaterExpenses).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.WaterExpenses, R.id.water_expenses_container,
                            R.id.water_expenses_error)) {

                        isNotInValidRange(R.id.WaterExpenses,R.id.water_expenses_container,
                                R.id.water_expenses_error,0,20000);
                    }
                }
            }
        });

        findViewById(R.id.GarbageExpenses).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.GarbageExpenses, R.id.garbage_expenses_container,
                            R.id.garbage_expenses_error)) {

                        isNotInValidRange(R.id.GarbageExpenses,R.id.garbage_expenses_container,
                                R.id.garbage_expenses_error,0,10000);
                    }
                }
            }
        });

        findViewById(R.id.HOAExpenses).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.HOAExpenses, R.id.hoa_expenses_container,
                            R.id.hoa_expenses_error)) {

                        isNotInValidRange(R.id.HOAExpenses,R.id.hoa_expenses_container,
                                R.id.hoa_expenses_error,0,10000);
                    }
                }
            }
        });

        findViewById(R.id.InsuranceExpenses).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.InsuranceExpenses, R.id.insurance_expenses_container,
                            R.id.insurance_expenses_error)) {

                        isNotInValidRange(R.id.InsuranceExpenses,R.id.insurance_expenses_container,
                                R.id.insurance_expenses_error,0,50000);
                    }
                }
            }
        });

        findViewById(R.id.PropertyTax).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.PropertyTax, R.id.property_tax_container,
                            R.id.property_tax_error)) {

                        isNotInValidRange(R.id.PropertyTax,R.id.property_tax_container,
                                R.id.property_tax_error,0,100000);
                    }
                }
            }
        });

        findViewById(R.id.OtherExpenses).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.OtherExpenses, R.id.other_expenses_container,
                            R.id.other_expenses_error)) {

                        isNotInValidRange(R.id.OtherExpenses,R.id.other_expenses_container,
                                R.id.other_expenses_error,0,100000);
                    }
                }
            }
        });
    }

    private void attachIncomeDetailsErrorMessageListeners() {
        findViewById(R.id.CapRate).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.CapRate, R.id.cap_rate_container,
                            R.id.cap_rate_error)) {

                        isNotInValidRange(R.id.CapRate,R.id.cap_rate_container,
                                R.id.cap_rate_error,-1000.,1000.);
                    }
                }
            }
        });

        findViewById(R.id.Rent).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.Rent, R.id.rent_container,
                            R.id.rent_error)) {

                        if(!isInvalidNumber(R.id.Rent,R.id.rent_container,
                                R.id.rent_error)){

                            isNotInValidRange(R.id.Rent,R.id.rent_container,
                                    R.id.rent_error,0,1000000);
                        }
                    }
                }
            }
        });

        findViewById(R.id.OtherIncome).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.OtherIncome, R.id.other_income_container,
                            R.id.other_income_error)) {

                        isNotInValidRange(R.id.OtherIncome,R.id.other_income_container,
                                R.id.other_income_error,0,1000000);
                    }
                }
            }
        });
    }

    private void attachAcquisitionDetailsErrorMessageListeners() {

        findViewById(R.id.PurchasePrice).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.PurchasePrice, R.id.purchase_price_container,
                            R.id.purchase_price_error)) {

                        if(!isInvalidNumber(R.id.PurchasePrice,R.id.purchase_price_container,
                                R.id.purchase_price_error)){

                            isNotInValidRange(R.id.PurchasePrice,R.id.purchase_price_container,
                                    R.id.purchase_price_error,0,5000000);
                        }
                    }
                }
            }
        });

        findViewById(R.id.ClosingCosts).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.ClosingCosts, R.id.closing_costs_container,
                            R.id.closing_cost_error)) {

                        isNotInValidRange(R.id.ClosingCosts,R.id.closing_costs_container,
                                    R.id.closing_cost_error,0,100000);
                    }
                }
            }
        });

        findViewById(R.id.AfterRepair).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.AfterRepair, R.id.after_repair_value_container,
                            R.id.after_repair_error)) {

                        if(!isInvalidNumber(R.id.AfterRepair,R.id.after_repair_value_container,
                                R.id.after_repair_error)){

                            isNotInValidRange(R.id.AfterRepair,R.id.after_repair_value_container,
                                    R.id.after_repair_error,0,10000000);
                        }
                    }
                }
            }
        });

        findViewById(R.id.RepairCosts).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.RepairCosts, R.id.repair_costs_container,
                            R.id.repair_cost_error)) {

                        isNotInValidRange(R.id.RepairCosts,R.id.repair_costs_container,
                                R.id.repair_cost_error,0,1000000);
                    }
                }
            }
        });

        findViewById(R.id.LoanAmount).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.LoanAmount, R.id.loan_amount_container,
                            R.id.loan_amount_error)) {

                        if(!isInvalidNumber(R.id.LoanAmount,R.id.loan_amount_container,
                                R.id.loan_amount_error)){

                            isNotInValidRange(R.id.LoanAmount,R.id.loan_amount_container,
                                    R.id.loan_amount_error,1,20000000);
                        }
                    }
                }
            }
        });

        findViewById(R.id.LenderCharges).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.LenderCharges, R.id.lender_charges_container,
                            R.id.lender_charges_error)) {

                        isNotInValidRange(R.id.LenderCharges,R.id.lender_charges_container,
                                R.id.lender_charges_error,0,100000);
                    }
                }
            }
        });

        findViewById(R.id.PointCharges).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.PointCharges, R.id.point_charges_container,
                            R.id.point_charges_error)) {

                        isNotInValidRange(R.id.PointCharges,R.id.point_charges_container,
                                R.id.point_charges_error,0,100000);
                    }
                }
            }
        });

        findViewById(R.id.InterestRate).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.InterestRate, R.id.interest_rate_container,
                            R.id.interest_rate_error)) {

                        isNotInValidRange(R.id.InterestRate,R.id.interest_rate_container,
                                R.id.interest_rate_error,0,100.);
                    }
                }
            }
        });

        findViewById(R.id.amortizationTime).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!ValidateFieldAndDisplayError(R.id.amortizationTime, R.id.amortization_time_container,
                            R.id.amortization_time_error)) {

                        if(!isInvalidNumber(R.id.amortizationTime,R.id.amortization_time_container,
                                R.id.amortization_time_error)){

                            isNotInValidRange(R.id.amortizationTime,R.id.amortization_time_container,
                                    R.id.amortization_time_error,1,100);
                        }
                    }
                }
            }
        });

        findViewById(R.id.WrapRadialGroup).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    if (!((RadioButton) findViewById(R.id.IntoLoanRButton)).isChecked() &&
                            !((RadioButton) findViewById(R.id.DirectlyRButton)).isChecked()) {
                        TextView target = findViewById(R.id.RadialHelper);
                        Utils.displayTextInLabel(R.string.invalid_input_radial,
                                ContextCompat.getColor(getApplicationContext(),
                                        R.color.error_text_red), target);
                    } else {
                        TextView target = findViewById(R.id.RadialHelper);
                        Utils.displayTextInLabel(R.string.empty_string,
                                ContextCompat.getColor(getApplicationContext(),
                                        R.color.design_default_color_surface), target);
                    }
                }
            }
        });
    }

    private void attachPropertyInfoErrorMessageListeners() {
        findViewById(R.id.ReportName).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    ValidateFieldAndDisplayError(R.id.ReportName, R.id.report_name_container,
                            R.id.report_name_error);
                }
            }
        });

        findViewById(R.id.Address).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    ValidateFieldAndDisplayError(R.id.Address, R.id.address_container,
                            R.id.address_error);
                }
            }
        });

        findViewById(R.id.City).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    ValidateFieldAndDisplayError(R.id.City, R.id.city_container,
                            R.id.city_error);
                }
            }
        });

        findViewById(R.id.ZIP).setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(!hasFocus){
                    ValidateFieldAndDisplayError(R.id.ZIP, R.id.zip_container,
                            R.id.zip_error);
                }
            }
        });
    }

    private void attachHelpListeners() {
        ((TextInputLayout)findViewById(R.id.purchase_price_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.purchase_price_help_title),
                                getResources().getString(R.string.purchase_price_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.after_repair_value_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.after_repair_value_help_title),
                                getResources().getString(R.string.after_repair_value_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.closing_costs_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.closing_costs_help_title),
                                getResources().getString(R.string.closing_costs_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.repair_costs_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.repair_costs_help_title),
                                getResources().getString(R.string.repair_costs_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.interest_rate_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.interest_rate_help_title),
                                getResources().getString(R.string.interest_rate_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.point_charges_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.point_charges_help_title),
                                getResources().getString(R.string.point_charges_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.amortization_time_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.amortization_time_help_title),
                                getResources().getString(R.string.amortization_time_help_message),
                                getSupportFragmentManager()));

        (findViewById(R.id.cash_purchase_help)).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.cash_purchase_help_title),
                        getResources().getString(R.string.cash_purchase_help_message),
                        getSupportFragmentManager()));

        (findViewById(R.id.interest_only_help)).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.interest_only_help_title),
                        getResources().getString(R.string.interest_only_help_message),
                        getSupportFragmentManager()));

        (findViewById(R.id.into_loan_help)).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.into_loan_help_title),
                        getResources().getString(R.string.into_loan_help_message),
                        getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.cap_rate_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.cap_rate_help_title),
                                getResources().getString(R.string.cap_rate_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.other_income_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.other_income_help_title),
                                getResources().getString(R.string.other_income_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.rent_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.rent_help_title),
                                getResources().getString(R.string.rent_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.water_expenses_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.water_expenses_help_title),
                                getResources().getString(R.string.water_expenses_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.electricity_expenses_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.electricity_expenses_help_title),
                                getResources().getString(R.string.electricity_expenses_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.garbage_expenses_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.garbage_expenses_help_title),
                                getResources().getString(R.string.garbage_expenses_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.sewer_expenses_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.sewer_expenses_help_title),
                                getResources().getString(R.string.sewer_expenses_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.insurance_expenses_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.insurance_expenses_help_title),
                                getResources().getString(R.string.insurance_expenses_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.hoa_expenses_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.hoa_expenses_help_title),
                                getResources().getString(R.string.hoa_expenses_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.property_tax_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.property_taxes_help_title),
                                getResources().getString(R.string.property_taxes_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.other_expenses_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.other_expenses_help_title),
                                getResources().getString(R.string.other_expenses_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.vacancy_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.vacancy_help_title),
                                getResources().getString(R.string.vacancy_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.maintenance_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.maintenance_help_title),
                                getResources().getString(R.string.maintenance_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.management_fees_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.management_fees_help_title),
                                getResources().getString(R.string.management_fees_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.cap_ex_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.cap_ex_help_title),
                                getResources().getString(R.string.cap_ex_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.income_inflation_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.income_inflation_help_title),
                                getResources().getString(R.string.income_inflation_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.expenses_inflation_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.expenses_inflation_help_title),
                                getResources().getString(R.string.expenses_inflation_help_message),
                                getSupportFragmentManager()));

        ((TextInputLayout)findViewById(R.id.property_value_inflation_container))
                .setEndIconOnClickListener
                        (new DisplayMessageOnClickListener(
                                getResources().getString(R.string.property_value_inflation_help_title),
                                getResources().getString(R.string.property_value_inflation_help_message),
                                getSupportFragmentManager()));
    }

    public void getImageFromFileSystem() {
        if (!checkReadStoragePermission()) return;

        Intent fetcher = new Intent(Intent.ACTION_GET_CONTENT);
        fetcher.setType("image/*");

        Intent picker = new Intent(Intent.ACTION_PICK);
        picker.setDataAndType(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "image/*");

        Intent chooserIntent = Intent.createChooser(fetcher, "Select Image");
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, new Intent[]{picker});

        startActivityForResult(chooserIntent, PICK_IMAGE);
    }

    private void setImageButtonByOutsideURI(@NonNull Uri uri) {
        ImageButton propertyImage = findViewById(R.id.ImageGetterButton);
        Drawable rawDrawable = Utils.getDrawableFromOutsideURI(getApplicationContext(), uri);

        Bitmap bitmap = Utils.convertDrawableToBitmap(rawDrawable);

        propertyImage.setImageDrawable(null);
        propertyImage.setImageDrawable(rawDrawable);
        model.setImageButton(rawDrawable);
        cacheBitmap(bitmap);
        model.setTempURI(this.tempImageURI);
    }

    private void cacheBitmap(Bitmap bitmap) {

        FileOutputStream out=null;
        try {
            String URI;

            File saveLocation = File.createTempFile(TEMP_FILE_NAME,null,getCacheDir());

            out=new FileOutputStream(saveLocation);

            bitmap.compress(Bitmap.CompressFormat.PNG, 100, out);

            URI = saveLocation.toURI().toString();
            this.tempImageURI = URI;

            Log.i("FL", "Temp Image File saved successfully to storage with uri "+tempImageURI);

        } catch (IOException e) {
            Log.e("FL", e.getMessage());
            Log.e("FL", Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
        }
        finally {
            if(out!=null) {
                try {
                    out.close();
                } catch (IOException e) {
                    Log.e("FL", e.getMessage());
                    Log.e("FL", Arrays.toString(e.getStackTrace()));
                }
            }
        }

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Uri imageUri;
        if (data != null) {
            imageUri = data.getData();
            if (requestCode == PICK_IMAGE) setImageButtonByOutsideURI(imageUri);
        }
    }

    private boolean checkReadStoragePermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted

            // No explanation needed; request the permission
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    getReadFilesPermissionCode());
            return false;
        } else return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (getReadFilesPermissionCode() == requestCode) {
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getImageFromFileSystem();
            } else {
                MessageFragment permissionWarning = makePermissionAlertFragment();
                permissionWarning.show(getSupportFragmentManager(), "PermissionWarningTag");
            }
        }
    }

    private MessageFragment makePermissionAlertFragment() {
        String message = getResources().getString(R.string.permission_warning_text);
        String title = getResources().getString(R.string.permission_warning_title);
        return MessageFragment.displayMessage(message, title);
    }

    private int getReadFilesPermissionCode() {
        return (getMyFileSystemPermission());
    }

    @Override
    public void onStart() {
        super.onStart();
        updateImageButton();
    }

    private void updateImageButton() {
        if(model.isSynced()){
            ((ImageButton) findViewById(R.id.ImageGetterButton))
                    .setImageDrawable(model.getImageButton());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        saveImageToModel();
    }

    private void saveImageToModel() {
        model.setImageButton(((ImageButton)findViewById(R.id.ImageGetterButton)).getDrawable());
    }

    @Override
    public void onResume() {
        super.onResume();
        setLoanUIEnabled(!((CheckBox) findViewById(R.id.CashCheckBox)).isChecked());
        ((CheckBox) findViewById(R.id.CashCheckBox))
                .setOnCheckedChangeListener(new CheckBox.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        setLoanUIEnabled(!isChecked);
                    }
                });
    }

    private void setLoanUIEnabled(boolean set) {
        findViewById(R.id.LenderCharges).setEnabled(set);
        findViewById(R.id.InterestRate).setEnabled(set);
        findViewById(R.id.WrapRadialGroup).setEnabled(set);
        findViewById(R.id.DirectlyRButton).setEnabled(set);
        findViewById(R.id.IntoLoanRButton).setEnabled(set);
        findViewById(R.id.LoanAmount).setEnabled(set);
        findViewById(R.id.PointCharges).setEnabled(set);
        findViewById(R.id.InterestOnlyCheckBox).setEnabled(set);
        findViewById(R.id.amortizationTime).setEnabled(set);
        clearTextLayoutError(R.id.loan_amount_container,R.id.loan_amount_error);
        clearTextLayoutError(R.id.lender_charges_container, R.id.lender_charges_error);
        clearTextLayoutError(R.id.interest_rate_container,R.id.interest_rate_error);
        clearTextLayoutError(R.id.point_charges_container,R.id.point_charges_error);
        clearTextLayoutError(R.id.amortization_time_container,R.id.amortization_time_error);
        TextView target = findViewById(R.id.RadialHelper);
        Utils.displayTextInLabel(R.string.empty_string,
                ContextCompat.getColor(getApplicationContext(),
                        R.color.design_default_color_surface), target);
    }

    /**Triggers after report to be edited is returned by the repository**/
    @Override
    public void onEditReportRetrieved(FullReport report){
        syncWithEditReport(report);
        synced = 1;//synced == true as bundle doest take booleans natively
    }

    private void syncWithEditReport(FullReport report) {

        NumberFormat formatter = NumberFormat.getInstance();

        ((EditText) findViewById(R.id.ReportName)).setText(report.reportName);
        ((EditText) findViewById(R.id.Address)).setText(report.address);
        ((EditText) findViewById(R.id.City)).setText(report.city);
        ((EditText) findViewById(R.id.ZIP)).setText(report.zip);

        Uri reportImageUri = Utils.resolveUriFromString(report.imageURI);
        Drawable reportDrawable = Utils.getDrawableFromURI(getApplicationContext(),reportImageUri);
        ((ImageButton)findViewById(R.id.ImageGetterButton)).setImageDrawable(reportDrawable);

        ((EditText) findViewById(R.id.MiscText)).setText(report.miscText);
        ((EditText) findViewById(R.id.PurchasePrice)).setText(formatter.format(report.purchasePrice));
        ((EditText) findViewById(R.id.AfterRepair)).setText(formatter.format(report.afterRepairValue));
        ((EditText) findViewById(R.id.RepairCosts)).setText(formatter.format(report.repairCosts));
        ((EditText) findViewById(R.id.ClosingCosts)).setText(formatter.format(report.closingCosts));

        ((CheckBox) findViewById(R.id.InterestOnlyCheckBox)).setChecked(report.interestOnly);
        ((CheckBox) findViewById(R.id.CashCheckBox)).setChecked(report.cashPurchase);

        if (!report.cashPurchase) {
            ((EditText) findViewById(R.id.InterestRate)).setText(formatter.format(report.interestRate));
            ((EditText) findViewById(R.id.LoanAmount)).setText(formatter.format(report.loanAmount));
            ((EditText) findViewById(R.id.LenderCharges)).setText(formatter.format(report.lenderCharges));
            ((EditText) findViewById(R.id.PointCharges)).setText(formatter.format(report.pointCharges));
            ((EditText) findViewById(R.id.amortizationTime)).setText(formatter.format(report.amortizationRate));
            ((RadioButton) findViewById(R.id.IntoLoanRButton)).setChecked(report.intoLoanR);
            ((RadioButton) findViewById(R.id.DirectlyRButton)).setChecked(!report.intoLoanR);
        }

        ((EditText) findViewById(R.id.CapRate)).setText(formatter.format(report.capRate));
        ((EditText) findViewById(R.id.Rent)).setText(formatter.format(report.rent));
        ((EditText) findViewById(R.id.OtherIncome)).setText(formatter.format(report.otherIncome));
        ((EditText) findViewById(R.id.ElectricityExpenses)).setText(formatter.format(report.electricity));
        ((EditText) findViewById(R.id.WaterExpenses)).setText(formatter.format(report.water));
        ((EditText) findViewById(R.id.SewerExpenses)).setText(formatter.format(report.sewer));
        ((EditText) findViewById(R.id.GarbageExpenses)).setText(formatter.format(report.garbage));
        ((EditText) findViewById(R.id.HOAExpenses)).setText(formatter.format(report.hoa));
        ((EditText) findViewById(R.id.InsuranceExpenses)).setText(formatter.format(report.insurance));
        ((EditText) findViewById(R.id.PropertyTax)).setText(formatter.format(report.propertyTax));
        ((EditText) findViewById(R.id.OtherExpenses)).setText(formatter.format(report.otherExpenses));
        ((EditText) findViewById(R.id.Maintenance)).setText(formatter.format(report.maintenance));
        ((EditText) findViewById(R.id.Vacancy)).setText(formatter.format(report.vacancy));
        ((EditText) findViewById(R.id.CapEx)).setText(formatter.format(report.capEX));
        ((EditText) findViewById(R.id.ManagementFees)).setText(formatter.format(report.management));
        ((EditText) findViewById(R.id.IncomeInflation)).setText(formatter.format(report.incomeInflation));
        ((EditText) findViewById(R.id.PropertyValueInflation)).setText(formatter.format(report.propertyValueInflation));
        ((EditText) findViewById(R.id.ExpensesInflation)).setText(formatter.format(report.expensesInflation));
    }

    public void nextScreen(View view) {
        findViewById(R.id.next_button).setEnabled(false);

        if(!checkPropertyInfo()){
            scrollToView(findViewById(R.id.property_info_card));
            findViewById(R.id.next_button).setEnabled(true);
            return;
        }

        if(!checkAcquisitionDetails()){
            scrollToView(findViewById(R.id.acquisition_details_card));
            findViewById(R.id.next_button).setEnabled(true);
            return;
        }

        if(!checkIncomeDetails()){
            scrollToView(findViewById(R.id.income_details_card));
            findViewById(R.id.next_button).setEnabled(true);
            return;
        }

        if(!checkFixedExpenses()){
            scrollToView(findViewById(R.id.fixed_expenses_card));
            findViewById(R.id.next_button).setEnabled(true);
            return;
        }

        if(!checkVariableExpenses()){
            scrollToView(findViewById(R.id.variable_expenses_card));
            findViewById(R.id.next_button).setEnabled(true);
            return;
        }

        if(!checkFutureAssumptions()){
            scrollToView(findViewById(R.id.future_assumptions_card));
            findViewById(R.id.next_button).setEnabled(true);
            return;
        }

        if (mode == EDIT) {
            editReportInDB();
        } else {
            uploadToDb();
        }

    }

    private void scrollToView(final View target) {
        final ScrollView scrollView = findViewById(R.id.top_scroller);

        scrollView.post(new Runnable() {
            @Override
            public void run() {
                scrollView.smoothScrollTo(0,target.getTop());
            }
        });
    }

    /**Triggers after reports are inserted/updated in repository**/
    public void onReportInserted(long id){
        sendReportToParent(id);
    }

    private void editReportInDB() {
        FullReport reportData = exportAsFullReport();
        reportData.id = getIntent().getLongExtra(ID,-1);
        repo.updateFullReport(reportData,this);
    }

    private void sendReportToParent(long reportParcel) {
        Intent data = new Intent();
        data.putExtra("ReportId", reportParcel);
        setResult(Activity.RESULT_OK, data);
        finish();
    }

    public String saveImageToStorage(byte[] image) {
        String URI = null;
        try {
            String name = encodeBitmapBytesToURI(image);

            File saveLocation = new File(getFilesDir(), name + ".bmp");

            if (saveLocation.exists() && saveLocation.canRead() && saveLocation.isFile()) {
                URI = saveLocation.toURI().toString();
            } else {
                FileOutputStream out =
                        openFileOutput(name + ".bmp", Context.MODE_PRIVATE);
                Utils.convertBytesToBitmap(image)
                        .compress(Bitmap.CompressFormat.PNG, 100, out);

                URI = saveLocation.toURI().toString();

                Log.i("FL", "File " + name + " saved successfully to storage");
            }

        } catch (IOException e) {
            Log.e("FL", e.getMessage());
            Log.e("FL", Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
        }
        return URI;
    }

    private static String encodeBitmapBytesToURI(byte[] image) {
        String name = "placeholder";
        try{
        Security.addProvider(new Blake2bProvider());
        final MessageDigest blake2b = MessageDigest.getInstance(Blake2b.BLAKE2_B_160);
        blake2b.update(image);
        name = Base64.encodeToString(blake2b.digest(), Base64.URL_SAFE);
        Log.d("FL", "URI generated: " + name);
        if (name.length() > 123)
            throw new IllegalArgumentException("name too long for file");
        } catch (NoSuchAlgorithmException e) {
            Log.e("FL", e.getMessage());
            Log.e("FL", Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
        } catch (Exception e) {
            Log.e("FL",e.getMessage());
            Log.e("FL", Arrays.toString(e.getStackTrace()));
            e.printStackTrace();
        }
        return name;
    }

    private void uploadToDb() {
        FullReport reportData = this.exportAsFullReport();
        repo.insertFullReport(reportData,this);
    }

    private FullReport exportAsFullReport(){

        long loan;
        int lenderCh;
        int pointCh;
        double interest;
        int amortizationTime;

        Drawable drawable = ((ImageButton)findViewById(R.id.ImageGetterButton)).getDrawable();
        Bitmap bitmap = Utils.convertDrawableToBitmap(drawable);
        String imageURI = saveImageToStorage(Utils.convertBitmapToBytes(bitmap));

        String reportName = ((EditText) findViewById(R.id.ReportName)).getText().toString();
        String address = ((EditText) findViewById(R.id.Address)).getText().toString();
        String city = ((EditText) findViewById(R.id.City)).getText().toString();
        String zip = ((EditText) findViewById(R.id.ZIP)).getText().toString();
        String miscText = ((EditText) findViewById(R.id.MiscText)).getText().toString();
        String purchasePrice = ((EditText) findViewById(R.id.PurchasePrice)).getText().toString();
        String afterRepairValue = ((EditText) findViewById(R.id.AfterRepair)).getText().toString();
        String repairCosts = ((EditText) findViewById(R.id.RepairCosts)).getText().toString();
        String closingCosts = ((EditText) findViewById(R.id.ClosingCosts)).getText().toString();
        boolean cashPurchase = ((CheckBox) findViewById(R.id.CashCheckBox)).isChecked();
        boolean interestOnly = ((CheckBox) findViewById(R.id.InterestOnlyCheckBox)).isChecked();
        String interestRate = ((EditText) findViewById(R.id.InterestRate)).getText().toString();
        String loanAmount = ((EditText) findViewById(R.id.LoanAmount)).getText().toString();
        String lenderCharges = ((EditText) findViewById(R.id.LenderCharges)).getText().toString();
        String pointCharges = ((EditText) findViewById(R.id.PointCharges)).getText().toString();
        boolean intoLoan = ((RadioButton) findViewById(R.id.IntoLoanRButton)).isChecked();
        String amortizationRate = ((EditText) findViewById(R.id.amortizationTime)).getText().toString();
        String capRate = ((EditText) findViewById(R.id.CapRate)).getText().toString();
        String rent = ((EditText) findViewById(R.id.Rent)).getText().toString();
        String otherIncome = ((EditText) findViewById(R.id.OtherIncome)).getText().toString();
        String electricity = ((EditText) findViewById(R.id.ElectricityExpenses)).getText().toString();
        String water = ((EditText) findViewById(R.id.WaterExpenses)).getText().toString();
        String sewer = ((EditText) findViewById(R.id.SewerExpenses)).getText().toString();
        String garbage = ((EditText) findViewById(R.id.GarbageExpenses)).getText().toString();
        String HOA = ((EditText) findViewById(R.id.HOAExpenses)).getText().toString();
        String insurance = ((EditText) findViewById(R.id.InsuranceExpenses)).getText().toString();
        String propertyTax = ((EditText) findViewById(R.id.PropertyTax)).getText().toString();
        String otherExpenses = ((EditText) findViewById(R.id.OtherExpenses)).getText().toString();
        String maintenance = ((EditText) findViewById(R.id.Maintenance)).getText().toString();
        String vacancy = ((EditText) findViewById(R.id.Vacancy)).getText().toString();
        String capEx = ((EditText) findViewById(R.id.CapEx)).getText().toString();
        String management = ((EditText) findViewById(R.id.ManagementFees)).getText().toString();
        String incomeInflation = ((EditText) findViewById(R.id.IncomeInflation)).getText().toString();
        String propertyValueInflation = ((EditText) findViewById(R.id.PropertyValueInflation)).getText().toString();
        String expensesInflation = ((EditText) findViewById(R.id.ExpensesInflation)).getText().toString();

        if (cashPurchase) {
            loan = 0;
            lenderCh = 0;
            pointCh = 0;
            interest = 0;
            amortizationTime=0;

        } else {
            loan = Long.parseLong(loanAmount);
            lenderCh = Integer.parseInt(lenderCharges);
            pointCh = Integer.parseInt(pointCharges);
            interest = Double.parseDouble(interestRate);
            amortizationTime = Integer.parseInt(amortizationRate);
        }

        FullReport r = new FullReport(reportName, address, city, zip,
                imageURI, miscText, Long.parseLong(purchasePrice),
                Long.parseLong(afterRepairValue),
                Integer.parseInt(repairCosts), Integer.parseInt(closingCosts),
                cashPurchase, Double.parseDouble(capRate),
                amortizationTime, Integer.parseInt(rent),
                Integer.parseInt(otherIncome), Integer.parseInt(electricity),
                Integer.parseInt(sewer), Integer.parseInt(water),
                Integer.parseInt(garbage), Integer.parseInt(HOA),
                Integer.parseInt(insurance), Integer.parseInt(propertyTax),
                Integer.parseInt(otherExpenses), Double.parseDouble(vacancy),
                Double.parseDouble(maintenance), Double.parseDouble(capEx),
                Double.parseDouble(management), Double.parseDouble(incomeInflation),
                Double.parseDouble(propertyValueInflation),
                Double.parseDouble(expensesInflation), interestOnly,
                loan, lenderCh, pointCh, interest, intoLoan);

        return r;
    }

    public int getMyFileSystemPermission() {
        return myFileSystemPermission;
    }

    boolean ValidateFieldAndDisplayError(int targetId, int containerId, int errorViewId) {
        TextView target = findViewById(targetId);
        boolean retr;
        TextInputLayoutGood container = findViewById(containerId);
        TextView errorView = findViewById(errorViewId);

        if (retr = hasInvalidInput(target)) {
            container.style(ContextCompat.getColor(getApplicationContext(),
                    R.color.error_text_red),R.style.InputLayoutError);//Hack

            Utils.displayTextInLabel(R.string.invalid_input_empty,
                    ContextCompat.getColor(getApplicationContext(),
                            R.color.error_text_red), errorView);
        }
        else {
            container.style(ContextCompat.getColor(getApplicationContext(),
                    R.color.accent_material_light), R.style.InputLayoutSuccess);

            Utils.displayTextInLabel("",
                    ContextCompat.getColor(getApplicationContext(),
                            R.color.design_default_color_surface), errorView);
        }
        return retr;
    }

    boolean isNotInValidRange(int targetId, int containerId, int errorViewId, double min, double max){
        boolean retr;
        TextView textView = findViewById(targetId);
        TextView errorView = findViewById(errorViewId);
        TextInputLayoutGood container = findViewById(containerId);
        double val = Double.parseDouble(textView.getText().toString());
        if(retr = val<min||val>max){
            container.style(ContextCompat.getColor(getApplicationContext(),
                    R.color.error_text_red),R.style.InputLayoutError);
            // HACK as by default you can not style TextInputLayout
            // programmatically GOOD JOB GOOGLE
            // why is this not part of the api by default ?!
            DecimalFormat format =new DecimalFormat("##############0.00");
            if(val>max) {
                Utils.displayTextInLabel("The maximum allowed value is "+format.format(max),
                        ContextCompat.getColor(getApplicationContext(),
                                R.color.error_text_red), errorView);
            }
            else {
                container.style(ContextCompat.getColor(getApplicationContext(),
                        R.color.accent_material_light), R.style.InputLayoutSuccess);

                Utils.displayTextInLabel("", ContextCompat.getColor
                        (getApplicationContext(),R.color.design_default_color_surface),
                        errorView);
            }
        }
        return retr;
    }

    boolean isInvalidNumber(int targetId, int containerId, int errorViewId){
        TextView target = findViewById(targetId);
        boolean retr;
        TextInputLayoutGood container = findViewById(containerId);
        TextView errorView = findViewById(errorViewId);

        if(retr = isInputZero(target)){
            container.style(ContextCompat.getColor(getApplicationContext(),
                    R.color.error_text_red),R.style.InputLayoutError);//hack

            Utils.displayTextInLabel(R.string.zero_string,
                    ContextCompat.getColor(getApplicationContext(),
                            R.color.error_text_red), errorView);
        }
        else {
            container.style(ContextCompat.getColor(getApplicationContext(),
                    R.color.accent_material_light), R.style.InputLayoutSuccess);

            Utils.displayTextInLabel("",
                    ContextCompat.getColor(getApplicationContext(),
                            R.color.design_default_color_surface), errorView);
        }
        return retr;
    }

    private static boolean isInputZero(TextView target) {
            return 0==Long.valueOf(target.getText().toString());
    }

    public static boolean hasInvalidInput(TextView target) {
        return target.getText() == null || !target.getText().toString().matches("^\\s*(\\S+\\s*)+");
    }

    private boolean checkPropertyInfo() {
        boolean valid = true;

        if (ValidateFieldAndDisplayError(R.id.ReportName, R.id.report_name_container,
                R.id.report_name_error)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.Address, R.id.address_container,R.id.address_error)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.City, R.id.city_container,R.id.city_error)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.ZIP, R.id.zip_container,R.id.zip_error)) {
            valid = false;
        }

        return valid;
    }

    private boolean checkAcquisitionDetails() {
        boolean valid = true;

        if (ValidateFieldAndDisplayError(R.id.PurchasePrice, R.id.purchase_price_container,
                R.id.purchase_price_error)) {
            valid = false;
        }
        else{
            if(isInvalidNumber(R.id.PurchasePrice,R.id.purchase_price_container,
                    R.id.purchase_price_error)){
            valid = false;
            }
            else {
                if(isNotInValidRange(R.id.PurchasePrice,R.id.purchase_price_container,
                        R.id.purchase_price_error,0,5000000)){
                    valid = false;
                }
            }
        }

        if (ValidateFieldAndDisplayError(R.id.ClosingCosts, R.id.closing_costs_container,
                R.id.closing_cost_error)) {
            valid = false;
        }
        else {
            if(isNotInValidRange(R.id.ClosingCosts,R.id.closing_costs_container,
                    R.id.closing_cost_error,0,100000)){
                valid = false;
            }
        }

        if (ValidateFieldAndDisplayError(R.id.AfterRepair, R.id.after_repair_value_container,
                R.id.after_repair_error)) {
            valid = false;
        }
        else{
            if(isInvalidNumber(R.id.AfterRepair,R.id.after_repair_value_container,
                    R.id.after_repair_error)){
                valid = false;
            }
            else {
                if(isNotInValidRange(R.id.AfterRepair,R.id.after_repair_value_container
                        ,R.id.after_repair_error,0,10000000)){
                    valid = false;
                }
            }
        }

        if (ValidateFieldAndDisplayError(R.id.RepairCosts, R.id.repair_costs_container,
                R.id.repair_cost_error)) {
            valid = false;
        }
        else {
            if(isNotInValidRange(R.id.RepairCosts, R.id.repair_costs_container,
                    R.id.repair_cost_error,0,1000000)){
                valid = false;
            }
        }

        if (!((CheckBox) findViewById(R.id.CashCheckBox)).isChecked()) {
            if (ValidateFieldAndDisplayError(R.id.LoanAmount, R.id.loan_amount_container,
                    R.id.loan_amount_error)) {
                valid = false;
            }
            else{
                if(isInvalidNumber(R.id.LoanAmount,R.id.loan_amount_container,
                        R.id.loan_amount_error)){
                    valid = false;
                }
                else {
                    if(isNotInValidRange(R.id.LoanAmount,R.id.loan_amount_container,
                            R.id.loan_amount_error,1,20000000)){
                        valid = false;
                    }
                }
            }

            if (ValidateFieldAndDisplayError(R.id.LenderCharges, R.id.lender_charges_container,
                    R.id.lender_charges_error)) {
                valid = false;
            }

            else {
                if(isNotInValidRange(R.id.LenderCharges, R.id.lender_charges_container,
                        R.id.lender_charges_error,0,100000)){
                    valid = false;
                }

            }
            if (ValidateFieldAndDisplayError(R.id.PointCharges, R.id.point_charges_container,
                    R.id.point_charges_error)) {
                valid = false;
            }
            else {
                if(isNotInValidRange(R.id.PointCharges, R.id.point_charges_container,
                        R.id.point_charges_error,0,100000)){
                    valid = false;
                }

            }
            if (ValidateFieldAndDisplayError(R.id.InterestRate, R.id.interest_rate_container,
                    R.id.interest_rate_error)) {
                valid = false;
            }
            else {
                if(isNotInValidRange(R.id.InterestRate, R.id.interest_rate_container
                        ,R.id.interest_rate_error,0,100.)){
                    valid = false;
                }
            }

            if (ValidateFieldAndDisplayError(R.id.amortizationTime, R.id.amortization_time_container
                    ,R.id.amortization_time_error)) {
                valid = false;
            }else{
                if(isInvalidNumber(R.id.amortizationTime,R.id.amortization_time_container
                        ,R.id.amortization_time_error)){
                    valid = false;
                }
                else if(isNotInValidRange(R.id.amortizationTime,R.id.amortization_time_container
                        ,R.id.amortization_time_error,1,100)){
                    valid = false;
                }
            }

            if (!((RadioButton) findViewById(R.id.IntoLoanRButton)).isChecked() &&
                    !((RadioButton) findViewById(R.id.DirectlyRButton)).isChecked()) {
                TextView target = findViewById(R.id.RadialHelper);
                Utils.displayTextInLabel(R.string.invalid_input_radial,
                        ContextCompat.getColor(getApplicationContext(),
                                R.color.error_text_red), target);
                valid = false;
            } else {
                TextView target = findViewById(R.id.RadialHelper);
                Utils.displayTextInLabel(R.string.empty_string,
                        ContextCompat.getColor(getApplicationContext(),
                                R.color.design_default_color_surface), target);
            }
        }
        else {
            clearTextLayoutError(R.id.loan_amount_container,R.id.loan_amount_error);
            clearTextLayoutError(R.id.lender_charges_container, R.id.lender_charges_error);
            clearTextLayoutError(R.id.interest_rate_container,R.id.interest_rate_error);
            clearTextLayoutError(R.id.point_charges_container,R.id.point_charges_error);
            clearTextLayoutError(R.id.amortization_time_container,R.id.amortization_time_error);
            TextView target = findViewById(R.id.RadialHelper);
            Utils.displayTextInLabel(R.string.empty_string,
                    ContextCompat.getColor(getApplicationContext(),
                            R.color.design_default_color_surface), target);
        }

        return valid;
    }

    private void clearTextLayoutError(int containerId, int errorViewId) {
        TextView errorView = findViewById(errorViewId);
        TextInputLayoutGood container = findViewById(containerId);

        container.setHintTextAppearance(R.style.InputLayoutNormal);

        Utils.displayTextInLabel("", ContextCompat.getColor
                        (getApplicationContext(), R.color.design_default_color_surface), errorView);
    }

    private boolean checkIncomeDetails() {
        boolean valid = true;

        if (ValidateFieldAndDisplayError(R.id.CapRate, R.id.cap_rate_container,R.id.cap_rate_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.CapRate, R.id.cap_rate_container,
                R.id.cap_rate_error,-1000,1000)){
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.Rent, R.id.rent_container,R.id.rent_error)) {
            valid = false;
        }
        else{
            if(isInvalidNumber(R.id.Rent,R.id.rent_container,R.id.rent_error)){
                valid = false;
            }
            else if(isNotInValidRange(R.id.Rent,R.id.rent_container,
                    R.id.rent_error,0,1000000)){
                valid = false;
            }
        }

        if (ValidateFieldAndDisplayError(R.id.OtherIncome, R.id.other_income_container,
                R.id.other_income_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.OtherIncome, R.id.other_income_container,
                R.id.other_income_error,0,1000000)){
            valid = false;
        }

        return valid;
    }

    private boolean checkFixedExpenses(){
        boolean valid = true;

        if (ValidateFieldAndDisplayError(R.id.ElectricityExpenses, R.id.electricity_expenses_container,
                R.id.electricity_expenses_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.ElectricityExpenses,
                R.id.electricity_expenses_container,
                R.id.electricity_expenses_error,0,50000)){
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.SewerExpenses, R.id.sewer_expenses_container,
                R.id.sewer_expenses_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.SewerExpenses, R.id.sewer_expenses_container,
                R.id.sewer_expenses_error,0,20000)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.WaterExpenses, R.id.water_expenses_container,
                R.id.water_expenses_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.WaterExpenses, R.id.water_expenses_container,
                R.id.water_expenses_error,0,20000)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.GarbageExpenses, R.id.garbage_expenses_container,
                R.id.garbage_expenses_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.GarbageExpenses, R.id.garbage_expenses_container,
                R.id.garbage_expenses_error,0,10000)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.HOAExpenses, R.id.hoa_expenses_container,
                R.id.hoa_expenses_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.HOAExpenses, R.id.hoa_expenses_container,
                R.id.hoa_expenses_error,0,10000)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.InsuranceExpenses, R.id.insurance_expenses_container
                ,R.id.insurance_expenses_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.InsuranceExpenses, R.id.insurance_expenses_container,
                R.id.insurance_expenses_error,0,50000)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.PropertyTax, R.id.property_tax_container,
                R.id.property_tax_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.PropertyTax, R.id.property_tax_container,
                R.id.property_tax_error,0,100000)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.OtherExpenses, R.id.other_expenses_container,
                R.id.other_expenses_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.OtherExpenses, R.id.other_expenses_container,
                R.id.other_expenses_error,0,100000)) {
            valid = false;
        }

        return valid;
    }

    private boolean checkVariableExpenses() {
        boolean valid = true;

        if (ValidateFieldAndDisplayError(R.id.Vacancy, R.id.vacancy_container,R.id.vacancy_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.Vacancy, R.id.vacancy_container,
                R.id.vacancy_error,0,100.)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.Maintenance, R.id.maintenance_container
                ,R.id.maintenance_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.Maintenance, R.id.maintenance_container,
                R.id.maintenance_error,0,100.)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.CapEx, R.id.cap_ex_container,R.id.cap_ex_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.CapEx, R.id.cap_ex_container,
                R.id.cap_ex_error,0,100.)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.ManagementFees, R.id.management_fees_container,
                R.id.management_fees_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.ManagementFees, R.id.management_fees_container,
                R.id.management_fees_error,0,100.)) {
            valid = false;
        }

        return valid;
    }

    private boolean checkFutureAssumptions(){

        boolean valid = true;

        if (ValidateFieldAndDisplayError(R.id.IncomeInflation, R.id.income_inflation_container,
                R.id.income_inflation_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.IncomeInflation, R.id.income_inflation_container,
                R.id.income_inflation_error,0,100.)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.PropertyValueInflation,
                R.id.property_value_inflation_container,
                R.id.property_value_inflation_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.PropertyValueInflation,
                R.id.property_value_inflation_container,
                R.id.property_value_inflation_error,0,100.)) {
            valid = false;
        }

        if (ValidateFieldAndDisplayError(R.id.ExpensesInflation, R.id.expenses_inflation_container,
                R.id.expenses_inflation_error)) {
            valid = false;
        }
        else if(isNotInValidRange(R.id.ExpensesInflation, R.id.expenses_inflation_container,
                R.id.expenses_inflation_error,0,100.)) {
            valid = false;
        }

        return valid;
    }

}
