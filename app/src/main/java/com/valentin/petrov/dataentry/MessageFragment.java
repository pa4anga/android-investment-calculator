package com.valentin.petrov.dataentry;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;

import com.valentin.petrov.investmentrentalcalculator.R;

public class MessageFragment extends DialogFragment {

    private String message;
    private String title;

    public static MessageFragment displayMessage(String title, String message) {
        MessageFragment instance = new MessageFragment();
        instance.setMessage(message);
        instance.setTitle(title);
        return instance;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        View layout = LayoutInflater.from(getContext())
                .inflate(R.layout.message_fragment_scrollable,null);

        ((TextView)layout.findViewById(R.id.title)).setText(title);
        ((TextView)layout.findViewById(R.id.message)).setText(message);

        builder.setView(layout);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            }
        });
        // Create the AlertDialog object and show it
        return builder.show();
    }

    private void setTitle(String title) {
        this.title = title;
    }

    private void setMessage(String message) {
        this.message = message;
    }
}