package com.valentin.petrov.displayreport;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

public class DisplayActivityViewModelFactory implements ViewModelProvider.Factory {
    private Application app;
    private long id;

    public DisplayActivityViewModelFactory(Application application, long id) {
        this.app = application;
        this.id=id;
    }


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new DisplayActivityViewModel(app, id);
    }
}
