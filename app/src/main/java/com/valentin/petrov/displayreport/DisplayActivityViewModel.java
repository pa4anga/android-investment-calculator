package com.valentin.petrov.displayreport;

import android.app.Application;
import android.graphics.drawable.Drawable;

import androidx.annotation.NonNull;
import androidx.arch.core.util.Function;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Transformations;

import com.valentin.petrov.database.FullReport;
import com.valentin.petrov.database.ReportsRepository;

import java.util.concurrent.Executors;

public class DisplayActivityViewModel extends AndroidViewModel {

    private LiveData<FullReport> rawReport;
    private LiveData<ProcessedReport> processedReport;
    private Drawable reportImage;

    public DisplayActivityViewModel(@NonNull Application application) {
        super(application);
    }

    public DisplayActivityViewModel(Application app, long id) {
        super(app);
        ReportsRepository repository = ReportsRepository.getInstance(app);
        rawReport= repository.getFullReportById(id);

        processedReport = Transformations.switchMap(this.rawReport, new Function<FullReport, LiveData<ProcessedReport>>() {
            @Override
            public LiveData<ProcessedReport> apply(FullReport report) {
                return DisplayActivityViewModel.this.processReport(report);
            }});
    }

    public LiveData<FullReport> getRawReport() {
        return rawReport;
    }

    private LiveData<ProcessedReport> processReport(final FullReport rawReport){
        final MutableLiveData<ProcessedReport> reportLiveData = new MutableLiveData<>();
        Executors.newSingleThreadExecutor().execute(new Runnable() {
            @Override
            public void run() {
                reportLiveData.postValue(new ProcessedReport(rawReport));
            }
        });
        return reportLiveData;
    }

    public LiveData<ProcessedReport> getProcessedReport() {
        return processedReport;
    }

    public Drawable getReportImage() {
        return reportImage;
    }

    public void setReportImage(Drawable reportImage) {
        this.reportImage = reportImage;
    }
}
