package com.valentin.petrov.displayreport;

import android.util.Log;

import com.valentin.petrov.database.FullReport;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class ProcessedReport {
    private double monthlyIncome;
    private double monthlyExpenses;
    private double loanMonthlyPayment;
    private ChartData chartData;
    private FullReport rawReport;

    public ProcessedReport(FullReport rawReport){
        this.rawReport = rawReport;
        monthlyIncome = calculateMonthlyIncome(rawReport);
        monthlyExpenses = calculateMonthlyExpenses(rawReport);
        if(!rawReport.cashPurchase){
            loanMonthlyPayment = calculateMonthlyLoanPayment(rawReport);
        }
        else {
            loanMonthlyPayment = 0;
        }
        chartData = generateChartData(rawReport,this.rawReport.amortizationRate+10);
    }

    public double[] getYearlyAnnualIncome() {
        return chartData.getAnnualIncome();
    }

    public double[] getYearlyAnnualExpenses() {
        return chartData.getAnnualExpenses();
    }

    public double[] getYearlyAnnualOperatingExpenses() {
        return chartData.getAnnualOperatingExpenses();
    }

    public double getCashOnCashReturn(){
        BigDecimal cashflow = BigDecimal.valueOf(this.getMonthlyCashflow());
        cashflow = cashflow.setScale(6,BigDecimal.ROUND_HALF_EVEN);
        FullReport report = this.getRawReport();
        BigDecimal cashExpended = BigDecimal.valueOf(calculateAcquisitionCosts(report));
        cashExpended = cashExpended.setScale(2, RoundingMode.HALF_EVEN);
        cashflow = cashflow.divide(cashExpended,4,BigDecimal.ROUND_HALF_UP);
        cashflow = cashflow.movePointRight(2);
        return cashflow.doubleValue();
    }

    private static double calculateAcquisitionCosts(FullReport report) {
        BigDecimal cashExpended = BigDecimal.valueOf(report.purchasePrice+report.repairCosts + report.closingCosts);
        cashExpended = cashExpended.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        if(!report.cashPurchase&&!report.intoLoanR){
            cashExpended = cashExpended.add(BigDecimal.valueOf(report.pointCharges +report.lenderCharges));
        }
        cashExpended = cashExpended.setScale(2,BigDecimal.ROUND_HALF_UP);
        return cashExpended.doubleValue();
    }

    public double[] getYearlyAnnualLoanPayments() {
        return chartData.getAnnualLoanPayments();
    }

    public double[] getYearlyAnnualCashflow() {
        return chartData.getAnnualCashflow();
    }

    public double[] getYearlyReturnOnInvestment() {
        return chartData.getReturnOnInvestment();
    }

    public double RepaymentAmount(){
        return chartData.getTotalLoanExpense();
    }

    public double[] getPropertyValue() {
        return chartData.getPropertyValue();
    }

    public double[] getEquity() {
        return chartData.getEquity();
    }

    public double[] getLoanBalance() {
        return chartData.getLoanBalance();
    }

    public static double getProFormaCapRate(FullReport rawReport){
        return calculateCapRate(rawReport.afterRepairValue,
                calculateNetOperatingIncome(calculateMonthlyIncome(rawReport),
                        calculateMonthlyOperatingExpenses(rawReport)));
    }

    public static double getPurchaseCapRate(FullReport rawReport){
        return calculateCapRate(rawReport.purchasePrice,
                calculateNetOperatingIncome(calculateMonthlyIncome(rawReport),
                        calculateMonthlyOperatingExpenses(rawReport)));
    }

    //Division by 0 possible warning
    private static double calculateCapRate(long assetValue, double operatingIncome) {
        BigDecimal acc= BigDecimal.valueOf(operatingIncome);
        acc = acc.setScale(6,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.divide(BigDecimal.valueOf(assetValue),BigDecimal.ROUND_HALF_EVEN);
        acc = acc.multiply(BigDecimal.valueOf(100));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    private static double calculateMonthlyExpenses(FullReport rawReport) {
        BigDecimal acc=BigDecimal
                .valueOf(calculateMonthlyOperatingExpenses(rawReport));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        if(!rawReport.cashPurchase){
            acc = acc.add(BigDecimal.valueOf(calculateMonthlyLoanPayment(rawReport)));
        }
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    //Division by 0 possible warning
    private static double getGrossRentMultiplier(FullReport rawReport){
        BigDecimal acc = BigDecimal.valueOf(rawReport.purchasePrice);
        acc = acc.setScale(4,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.divide(BigDecimal.valueOf(rawReport.rent*12),
                2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    //Division by 0 possible warning INTENTIONAL
    public double getDeptCoverageRatio(){
        BigDecimal acc = BigDecimal.valueOf(calculateNetOperatingIncome(rawReport));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.divide(BigDecimal.valueOf(this.loanMonthlyPayment),
                2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    public static double getEquity(FullReport rawReport){
        return calculateEquity(rawReport.afterRepairValue,calculateFullLoan(rawReport));
    }

    private static double calculateEquity(double value, double loan) {
        BigDecimal acc = BigDecimal.valueOf(value);
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.subtract(BigDecimal.valueOf(loan));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    //Division by 0 possible warning
    public static double calculateMonthlyLoanPayment(FullReport rawReport) {
        if(rawReport.interestOnly)return calculateLoanInterestOnlyPayment(rawReport);
        if(rawReport.interestRate==0){
            return calculatePaymentForNoInterest(rawReport);
        }

        //loan amount*loan percentage/(1-(1+interest percentage)^-payments total)
        BigDecimal one= BigDecimal.ONE;
        BigDecimal interestPercent = BigDecimal.valueOf(rawReport.interestRate);
        interestPercent = interestPercent
                .setScale(6,BigDecimal.ROUND_HALF_EVEN);
        interestPercent = interestPercent.movePointLeft(2);
        //interestPercent = interestPercent
               // .divide(BigDecimal.valueOf(12),BigDecimal.ROUND_HALF_EVEN);

        BigDecimal interest= BigDecimal
                .valueOf(rawReport.loanAmount);
        interest = interest.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        interest = interest.multiply(interestPercent);


        BigDecimal acc= one.add(interestPercent);
        acc = acc.pow(-rawReport.amortizationRate,new MathContext(50));
        acc = one.subtract(acc);
        acc = interest.divide(acc,2,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.divide(BigDecimal.valueOf(12),2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    private static double calculatePaymentForNoInterest(FullReport rawReport) {
        BigDecimal acc = BigDecimal.valueOf(rawReport.loanAmount);
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.divide(new BigDecimal(rawReport.amortizationRate),2,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.divide(BigDecimal.valueOf(12),BigDecimal.ROUND_HALF_EVEN);
        return  acc.doubleValue();
    }

    public double getMonthlyCashflow(){
        return calculateCashflow(this.monthlyIncome,this.monthlyExpenses);
    }

    public static double calculateNetOperatingIncome(FullReport rawReport){
        return calculateNetOperatingIncome(calculateMonthlyIncome(rawReport),
                calculateMonthlyOperatingExpenses(rawReport));
    }

    private static double calculateNetOperatingIncome(double monthlyIncome, double operatingExpenses) {
        BigDecimal acc=BigDecimal.valueOf(monthlyIncome);
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.subtract(BigDecimal.valueOf(operatingExpenses));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return  acc.doubleValue();
    }

    public static double calculateCashflow(double income, double expense){
        BigDecimal acc = new BigDecimal(income);
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.subtract(new BigDecimal(expense));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    public double getTotalCashNeeded(){
        return calculateTotalCashNeeded(rawReport);
    }

    public static double calculateTotalCashNeeded(FullReport rawReport){
        if(rawReport.cashPurchase)return calculateAcquisitionCosts(rawReport);

        BigDecimal acc=BigDecimal.valueOf(calculateAcquisitionCosts(rawReport));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        acc=acc.subtract(new BigDecimal(rawReport.loanAmount));

        if(acc.doubleValue()<0)return 0;
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    public double getNetOperatingIncome(){
        return calculateNetOperatingIncome(rawReport);
    }

    private static double calculateMonthlyOperatingExpenses(FullReport rawReport) {
        BigDecimal acc=new BigDecimal(calculateFixedExpenses(rawReport));
        acc = acc.setScale(6,BigDecimal.ROUND_HALF_EVEN);
        acc=acc.add(new BigDecimal(calculateCapEx(rawReport)));
        acc=acc.add(new BigDecimal(calculateVacancyCosts(rawReport)));
        acc=acc.add(new BigDecimal(calculateManagementCosts(rawReport)));
        acc=acc.add(new BigDecimal(calculateMaintenanceCosts(rawReport)));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    public static double calculateFullLoan(FullReport rawReport){
        if(rawReport.cashPurchase)return 0;
        if(!rawReport.intoLoanR)return rawReport.loanAmount;
        BigDecimal acc = BigDecimal.valueOf(rawReport.loanAmount);
        acc = acc.setScale(6,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.add(new BigDecimal(rawReport.lenderCharges));
        acc = acc.add(new BigDecimal(rawReport.pointCharges));
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    public static double calculateLoanInterestOnlyPayment(FullReport rawReport){
       return calculateLoanInterest(rawReport.interestRate,calculateFullLoan(rawReport));
    }

    private static double calculateLoanInterest(double interestRate, double loan) {
        BigDecimal acc = BigDecimal.valueOf
                (calculatePercentOfValue(interestRate,loan));
        acc = acc.setScale(6,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.divide(BigDecimal.valueOf(12),2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    public static double calculateMaintenanceCosts(FullReport rawReport) {
        return calculatePercentOfValue(rawReport.maintenance,calculateMonthlyIncome(rawReport));
    }

    public static double calculateManagementCosts(FullReport rawReport) {
        return calculatePercentOfValue(rawReport.management,calculateMonthlyIncome(rawReport));
    }

    public static double calculateVacancyCosts(FullReport rawReport) {
        return calculatePercentOfValue(rawReport.vacancy,calculateMonthlyIncome(rawReport));
    }

    public static double calculateCapEx(FullReport rawReport) {
        return calculatePercentOfValue(rawReport.capEX,calculateMonthlyIncome(rawReport));
    }

    private static double calculatePercentOfValue(double percent, double value) {
        BigDecimal acc = new BigDecimal(value);
        acc = acc.setScale(6,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.multiply(new BigDecimal(percent));
        acc = acc.movePointLeft(2);
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    private static double calculateFixedExpenses(FullReport rawReport) {
        return rawReport.electricity+rawReport.water+rawReport.sewer
                +rawReport.insurance+rawReport.otherExpenses+rawReport.hoa
                +rawReport.propertyTax;
    }

    public static double calculateMonthlyIncome(FullReport rawReport) {
        return rawReport.otherIncome+rawReport.rent;
    }

    public static double calculateReturnOnInvestment(FullReport rawReport){
        return calculateReturnOnInvestmentPercent(rawReport.afterRepairValue,
                calculateAcquisitionCosts(rawReport));
    }

    private static double calculateReturnOnInvestmentPercent
            (double currentValueGenerated, double acquisitionCosts) {
        BigDecimal acc= BigDecimal.valueOf(currentValueGenerated);
        acc = acc.setScale(6,BigDecimal.ROUND_HALF_EVEN);
        acc = acc.subtract(BigDecimal.valueOf(acquisitionCosts));
        acc = acc.divide(BigDecimal.valueOf(acquisitionCosts),BigDecimal.ROUND_HALF_EVEN);
        acc = acc.movePointRight(2);
        acc = acc.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        return acc.doubleValue();
    }

    public double getMonthlyIncome() {
        return monthlyIncome;
    }

    public double getMonthlyExpenses() {
        return monthlyExpenses;
    }

    public double getLoanMonthlyPayment() {
        return this.loanMonthlyPayment;
    }

    private ChartData generateChartData(FullReport rawReport, int years){

        //Setup
        BigDecimal LoanExpenseTotal = BigDecimal.ZERO;
        LoanExpenseTotal = LoanExpenseTotal
                .setScale(2,BigDecimal.ROUND_HALF_EVEN);

        BigDecimal loanPayment=BigDecimal
                .valueOf(this.loanMonthlyPayment);
        loanPayment = loanPayment.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        loanPayment = loanPayment.multiply(BigDecimal.valueOf(12));

        BigDecimal propertyValue=new BigDecimal(rawReport.afterRepairValue);
        propertyValue = propertyValue.setScale(2,BigDecimal.ROUND_HALF_EVEN);

        BigDecimal operatingExpenses =
                BigDecimal.valueOf(calculateMonthlyOperatingExpenses(rawReport));
        operatingExpenses = operatingExpenses
                .setScale(2,BigDecimal.ROUND_HALF_EVEN);
        operatingExpenses = operatingExpenses.multiply(BigDecimal.valueOf(12));

        BigDecimal loanBalance = BigDecimal.valueOf(calculateFullLoan(rawReport));
        loanBalance = loanBalance.setScale(2,BigDecimal.ROUND_HALF_EVEN);

        BigDecimal income = BigDecimal.valueOf(this.getMonthlyIncome()*12);
        income = income.setScale(2,BigDecimal.ROUND_HALF_EVEN);

        ChartData store = new ChartData(years);

        BigDecimal incomeInflationMultiplier=
                BigDecimal.valueOf(rawReport.incomeInflation);
        incomeInflationMultiplier = incomeInflationMultiplier.movePointLeft(2);
        incomeInflationMultiplier = incomeInflationMultiplier.add(BigDecimal.ONE);

        BigDecimal expensesInflationMultiplier=
                BigDecimal.valueOf(rawReport.expensesInflation);
        expensesInflationMultiplier = expensesInflationMultiplier.movePointLeft(2);
        expensesInflationMultiplier = expensesInflationMultiplier.add(BigDecimal.ONE);

        BigDecimal propertyValueInflationMultiplier=
                BigDecimal.valueOf(rawReport.propertyValueInflation);

        propertyValueInflationMultiplier = propertyValueInflationMultiplier
                .movePointLeft(2);

        propertyValueInflationMultiplier = propertyValueInflationMultiplier
                .add(BigDecimal.ONE);

        //First year special case
        double interest;
        BigDecimal totalExpenses=operatingExpenses;

        if(!rawReport.cashPurchase){
            interest=calculatePercentOfValue
                    (rawReport.interestRate,loanBalance.doubleValue());

            if(rawReport.interestOnly){
                totalExpenses= operatingExpenses.add(BigDecimal.valueOf(interest));
                LoanExpenseTotal = totalExpenses.add(BigDecimal.valueOf(interest));
            }
            else{
                if(loanBalance.doubleValue()>=1){//loan not repaid
                    totalExpenses = operatingExpenses.add(loanPayment);
                    LoanExpenseTotal = LoanExpenseTotal.add(loanPayment);

                    BigDecimal principalPayment = loanPayment
                            .subtract(BigDecimal.valueOf(interest));
                    loanBalance = loanBalance.subtract(principalPayment);
                    if(loanBalance.doubleValue()<1){
                        loanPayment=BigDecimal.ZERO;
                    }
                }
            }
        }


        double equity = calculateEquity
                (propertyValue.doubleValue(), loanBalance.doubleValue());

        double cashflow = calculateCashflow
                (income.doubleValue(),totalExpenses.doubleValue());

        BigDecimal totalDirectProfit = new BigDecimal(cashflow);
        totalDirectProfit = totalDirectProfit.setScale(2,BigDecimal.ROUND_HALF_EVEN);
        BigDecimal totalProfitGenerated = totalDirectProfit.add(propertyValue);

        double returnOnInvestment = calculateReturnOnInvestmentPercent
                (totalProfitGenerated.doubleValue(),
                        calculateAcquisitionCosts(rawReport));

        store.insertDataForOneYear(1,cashflow,totalExpenses.doubleValue(),
                income.doubleValue(),loanPayment.doubleValue(),
                operatingExpenses.doubleValue(),equity,loanBalance.doubleValue(),
                propertyValue.doubleValue(),returnOnInvestment);

        //Rest of years
        for(int i=2;i<=years;i++){
           income = income.multiply(incomeInflationMultiplier);
           income = income.setScale(2,BigDecimal.ROUND_HALF_EVEN);

           operatingExpenses = operatingExpenses
                   .multiply(expensesInflationMultiplier);
           operatingExpenses = operatingExpenses
                   .setScale(2,BigDecimal.ROUND_HALF_EVEN);

           propertyValue = propertyValue.multiply(propertyValueInflationMultiplier);
           propertyValue = propertyValue
                   .setScale(2,BigDecimal.ROUND_HALF_EVEN);

            totalExpenses=operatingExpenses;//assume loan payment

            if(!rawReport.cashPurchase){
                interest=calculatePercentOfValue
                        (rawReport.interestRate,loanBalance.doubleValue());

                if(rawReport.interestOnly){
                    totalExpenses= operatingExpenses.add(BigDecimal.valueOf(interest));
                    LoanExpenseTotal = totalExpenses.add(BigDecimal.valueOf(interest));
                }
                else{
                    if(loanBalance.doubleValue()>=1){//loan not repaid
                        if(loanBalance.doubleValue()<loanPayment.doubleValue()){
                            loanPayment=loanBalance;//last payment
                        }
                        totalExpenses = operatingExpenses.add(loanPayment);
                        LoanExpenseTotal = LoanExpenseTotal.add(loanPayment);
                        Log.d("FM","Year ="+i);
                        Log.d("FM","Total interest ="+LoanExpenseTotal.toString());
                        Log.d("FM","Loan Payment ="+loanPayment.toString());
                        Log.d("FM","Interest =" +interest);
                        Log.d("FM","Loan Balance =" +loanBalance.toString());

                        BigDecimal principalPayment = loanPayment
                                .subtract(BigDecimal.valueOf(interest));
                        loanBalance = loanBalance.subtract(principalPayment);
                        if(loanBalance.doubleValue()<1){
                            Log.d("FM","Loan Repaid on cycle " + i);
                            loanPayment=BigDecimal.ZERO;
                            loanBalance=BigDecimal.ZERO;
                        }
                    }
                }
            }

            equity = calculateEquity
                    (propertyValue.doubleValue(), loanBalance.doubleValue());

            totalDirectProfit = totalDirectProfit.add(BigDecimal.valueOf(cashflow));
            totalProfitGenerated = totalDirectProfit.add(propertyValue);

            returnOnInvestment = calculateReturnOnInvestmentPercent
                    (totalProfitGenerated.doubleValue(), calculateAcquisitionCosts(rawReport));

            cashflow = calculateCashflow
                    (income.doubleValue(),totalExpenses.doubleValue());

            store.insertDataForOneYear(i,cashflow,totalExpenses.doubleValue(),
                    income.doubleValue(),loanPayment.doubleValue(),
                    operatingExpenses.doubleValue(),equity,loanBalance.doubleValue(),
                    propertyValue.doubleValue(),returnOnInvestment);
        }

        store.totalLoanExpense = LoanExpenseTotal.doubleValue();

        return store;

    }

    public FullReport getRawReport() {
        return rawReport;
    }

    private class ChartData {
        private double[] annualIncome;
        private double[] annualExpenses;
        private double[] annualOperatingExpenses;
        private double[] annualLoanPayments;
        private double[] annualCashflow;
        private double[] returnOnInvestment;
        private double[] propertyValue;
        private double[] equity;
        private double[] loanBalance;
        private double totalLoanExpense;

        ChartData(int years){
            this.annualCashflow=new double[years];
            this.annualExpenses =new double[years];
            this.annualIncome=new double[years];
            this.annualLoanPayments=new double[years];
            this.annualOperatingExpenses =new double[years];
            this.equity=new double[years];
            this.loanBalance=new double[years];
            this.propertyValue=new double[years];
            this.returnOnInvestment=new double[years];
        }

        void insertDataForOneYear(int year,double cashflow, double totalExpenses,
                                  double income, double loanPayment,
                                  double operatingExpenses,double equity,
                                  double loanBalance, double propertyValue,
                                  double returnOnInvestment){
            this.annualCashflow[year-1]=cashflow;
            this.annualExpenses[year-1]=totalExpenses;
            this.annualIncome[year-1]=income;
            this.annualLoanPayments[year-1]=loanPayment;
            this.annualOperatingExpenses[year-1]=operatingExpenses;
            this.equity[year-1]=equity;
            this.loanBalance[year-1]=loanBalance;
            this.propertyValue[year-1]=propertyValue;
            this.returnOnInvestment[year-1]=returnOnInvestment;
        }

        public double[] getAnnualIncome() {
            return annualIncome;
        }

        public double[] getAnnualExpenses() {
            return annualExpenses;
        }

        public double[] getAnnualOperatingExpenses() {
            return annualOperatingExpenses;
        }

        public double[] getAnnualLoanPayments() {
            return annualLoanPayments;
        }

        public double[] getAnnualCashflow() {
            return annualCashflow;
        }

        public double[] getReturnOnInvestment() {
            return returnOnInvestment;
        }

        public double[] getPropertyValue() {
            return propertyValue;
        }

        public double[] getEquity() {
            return equity;
        }

        public double[] getLoanBalance() {
            return loanBalance;
        }

        public double getTotalLoanExpense() {
            return totalLoanExpense;
        }
    }

}
