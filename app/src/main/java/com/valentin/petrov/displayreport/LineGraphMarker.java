package com.valentin.petrov.displayreport;

import android.content.Context;
import android.widget.TextView;

import com.github.mikephil.charting.components.IMarker;
import com.github.mikephil.charting.components.MarkerView;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.utils.MPPointF;
import com.valentin.petrov.investmentrentalcalculator.R;
import com.valentin.petrov.investmentrentalcalculator.Utils;

public class LineGraphMarker extends MarkerView implements IMarker {

    private TextView markerView;
    private int datasetSize;
    private static final int LEFT=0;
    private static final int RIGHT=1;
    private static final int UP=2;
    private static final int DOWN=3;

    private int offsetXFlag;
    private int offsetYFlag;
    private float maxY;

    public LineGraphMarker(Context context, int layoutResource, int datasetSize, float maxY) {
        super(context, layoutResource);
        this.datasetSize = datasetSize;
        this.maxY=maxY;

        markerView = findViewById(R.id.marker_text);
    }

    @Override
    public void refreshContent(Entry e, Highlight highlight) {
        markerView.setText(Utils.formatToMoney(e.getY()));
        super.refreshContent(e, highlight);
        if(e.getX()>datasetSize/2){
            offsetXFlag = LEFT;
        }
        else {
            offsetXFlag = RIGHT;
        }

        if(e.getY()>(maxY/10)*9){
            offsetYFlag = DOWN;
        }
        else {
            offsetYFlag = UP;
        }
    }

    @Override
    public MPPointF getOffset() {

        MPPointF offset;

        float y;

        if(offsetYFlag == UP){
            y= -getHeight();
        }
        else {
            y=0;

        }
        if(offsetXFlag == RIGHT){//Align to the Right of entry
            offset = new MPPointF(0, y);
        }
        else {//Align to the Left of entry
            offset = new MPPointF(-getWidth(), y);
        }

        return offset;
    }
}
