package com.valentin.petrov.displayreport;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.graphics.pdf.PdfDocument;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.valentin.petrov.database.FullReport;
import com.valentin.petrov.dataentry.DataEntryActivity;
import com.valentin.petrov.dataentry.DisplayMessageOnClickListener;
import com.valentin.petrov.investmentrentalcalculator.R;
import com.valentin.petrov.investmentrentalcalculator.Utils;

import java.io.IOException;
import java.io.OutputStream;
import java.lang.ref.WeakReference;
import java.security.InvalidParameterException;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static android.content.Intent.ACTION_CREATE_DOCUMENT;
import static com.valentin.petrov.dataentry.DataEntryActivity.EDIT;
import static com.valentin.petrov.dataentry.DataEntryActivity.MODE;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateCapEx;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateFullLoan;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateMaintenanceCosts;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateManagementCosts;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateMonthlyIncome;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateNetOperatingIncome;
import static com.valentin.petrov.displayreport.ProcessedReport.calculateVacancyCosts;
import static com.valentin.petrov.displayreport.ProcessedReport.getProFormaCapRate;
import static com.valentin.petrov.displayreport.ProcessedReport.getPurchaseCapRate;

public class DisplayReportActivity extends AppCompatActivity {

    public static final String ID = "ID";
    private static final int FILE_SAVE_LOCATION_ACTIVITY_CODE = 2610;
    private static final int OTHER_INCOME_COLOR = Color.parseColor("#550a5c");
    private static final int RENT_COLOR = Color.parseColor("#17ff48");
    private static final int MANAGEMENT_COLOR = Color.parseColor("#EB595A");
    private static final int VACANCY_COLOR = Color.parseColor("#DECF3F");
    private static final int MAINTENANCE_COLOR = Color.parseColor("#B276B2");
    private static final int CAP_EX_COLOR = Color.parseColor("#FA7F52");
    private static final int ELECTRICITY_COLOR = Color.parseColor("#F17CB0");
    private static final int WATER_COLOR = Color.parseColor("#60BD68");
    private static final int SEWER_COLOR = Color.parseColor("#e2ea5b");
    private static final int INSURANCE_COLOR = Color.parseColor("#5DA5DA");
    private static final int OTHER_EXPENSES_COLOR = Color.parseColor("#5b4f7d");
    private static final int HOA_COLOR = Color.GRAY;
    private static final int LOAN_PAYMENT_COLOR = ColorTemplate.JOYFUL_COLORS[0];
    private static final int PROPERTY_TAX_COLOR = Color.parseColor("#3fe5c8");
    DisplayActivityViewModel model;
    ValueFormatter percentFormatter = new ValueFormatter() {
        @Override
        public String getAxisLabel(float value, AxisBase axis) {
            return super.getAxisLabel(value, axis)+"%";
        }
    };

    private final Observer<FullReport> rawReportObserver = new Observer<FullReport>() {
        @Override
        public void onChanged(FullReport report) {
            if(report!=null){
                syncTitleCard(report);
                syncIncomePie(report, (PieChart)findViewById(R.id.income_pie));
            }
        }
    };

    private final Observer<ProcessedReport> processedReportObserver = new Observer<ProcessedReport>() {
        @Override
        public void onChanged(ProcessedReport report) {
            if(report!=null){
                syncFinancialOverviewCard(report);
                syncExpensesPie(report, (PieChart)findViewById(R.id.expenses_pie));
                syncCashflowGraphCard(report);
                syncEquityCard(report);
                syncRoiCard(report);
                syncLoanCard(report);
                findViewById(R.id.pdf_button).setEnabled(true);
            }
        }
    };

    private void syncLoanCard(ProcessedReport report) {
        if(report.getRawReport().cashPurchase){
            findViewById(R.id.loan_details_card).setVisibility(View.GONE);
            return;
        }

        ((TextView)findViewById(R.id.loan_amount))
                .setText(Utils.formatToMoney(
                        (calculateFullLoan(report.getRawReport()))));

        ((TextView)findViewById(R.id.loan_payment))
                .setText(Utils.formatToMoney(
                        report.getLoanMonthlyPayment()));

        ((TextView)findViewById(R.id.dept_coverage_ratio))
                .setText(Utils.formatToPercent((report.getDeptCoverageRatio())));

        ((TextView)findViewById(R.id.repayment_amount))
                .setText(Utils.formatToMoney(report.RepaymentAmount()));
    }

    private void syncRoiCard(ProcessedReport report) {
        syncRoiChart(report, (LineChart)findViewById(R.id.roi_graph_chart));
        generateAssumptionMessageRoi(report);
    }

    private void syncRoiChart(ProcessedReport report, LineChart chart) {
        LineDataSet returnOnInvestment = generateReturnOnInvestmentDataset(report);
        LineData lineData = new LineData(returnOnInvestment);

        chart.getDescription().setEnabled(false);

        chart.getLegend().setForm(Legend.LegendForm.CIRCLE);
        chart.getLegend().setXEntrySpace(8f);
        chart.getLegend().setWordWrapEnabled(true);
        chart.setExtraBottomOffset(8);

        chart.getXAxis().setAvoidFirstLastClipping(true);
        chart.getAxisRight().setDrawLabels(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.TOP);
        if(returnOnInvestment.getYMin()<0) {
            chart.getAxisRight().setAxisMinimum(returnOnInvestment.getYMin()-10);
            chart.getAxisLeft().setAxisMinimum(returnOnInvestment.getYMin()-10);
        }
        else {
            chart.getAxisRight().setAxisMinimum(-10);
            chart.getAxisLeft().setAxisMinimum(-10);
        }
        chart.getAxisLeft().setValueFormatter(percentFormatter);

        chart.setScaleEnabled(false);

        chart.setData(lineData);
        chart.setMarker(new LineGraphMarker(getApplicationContext(),
                R.layout.line_graph_marker,returnOnInvestment.getEntryCount(),
                chart.getYMax()));

        chart.invalidate();
    }

    private static void syncRoiChartPdf(ProcessedReport report, LineChart chart) {
        LineDataSet returnOnInvestment = generateReturnOnInvestmentDataset(report);
        LineData lineData = new LineData(returnOnInvestment);

        chart.getDescription().setEnabled(false);

        chart.getLegend().setForm(Legend.LegendForm.CIRCLE);
        chart.getLegend().setXEntrySpace(8f);
        chart.getLegend().setWordWrapEnabled(true);
        chart.setExtraBottomOffset(8);

        chart.getXAxis().setAvoidFirstLastClipping(true);
        chart.getAxisRight().setDrawLabels(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.TOP);
        if(returnOnInvestment.getYMin()<0) {
            chart.getAxisRight().setAxisMinimum(returnOnInvestment.getYMin()-10);
            chart.getAxisLeft().setAxisMinimum(returnOnInvestment.getYMin()-10);
        }
        else {
            chart.getAxisRight().setAxisMinimum(-10);
            chart.getAxisLeft().setAxisMinimum(-10);
        }

        ValueFormatter percentFormatter = new ValueFormatter() {
            @Override
            public String getAxisLabel(float value, AxisBase axis) {
                return super.getAxisLabel(value, axis)+"%";
            }
        };

        chart.getAxisLeft().setValueFormatter(percentFormatter);

        chart.setScaleEnabled(false);

        chart.setData(lineData);

        chart.invalidate();
    }

    private void generateAssumptionMessageRoi(ProcessedReport report) {
        Format format = new DecimalFormat("##0.00");
        String assumptionMessage="Assuming Property Value inflation of "+
                format.format(report.getRawReport().propertyValueInflation) +
                "%,  Income inflation of " +
                format.format(report.getRawReport().incomeInflation) +
                "% and Expenses inflation of " +
                format.format(report.getRawReport().expensesInflation) + "%";
        ((TextView)findViewById(R.id.assumption_text_roi))
                .setText(assumptionMessage);
    }

    private void syncEquityCard(ProcessedReport report) {
        syncEquityChart(report, (LineChart)findViewById(R.id.equity_graph_chart));
        generateAssumptionMessageEquity(report);
    }

    private void syncEquityChart(ProcessedReport report, LineChart chart) {
        LineDataSet equity = generateEquityDataset(report);
        LineDataSet propertyValue = generatePropertyValueDataset(report);
        LineData lineData = new LineData(equity,propertyValue);

        if(!report.getRawReport().cashPurchase){
            LineDataSet loanBalance = generateLoanBalanceDataset(report);
            lineData.addDataSet(loanBalance);
        }

        chart.getDescription().setEnabled(false);

        chart.getLegend().setForm(Legend.LegendForm.CIRCLE);
        chart.getLegend().setXEntrySpace(8f);
        chart.getLegend().setWordWrapEnabled(true);
        chart.setExtraBottomOffset(8);

        chart.getXAxis().setAvoidFirstLastClipping(true);
        chart.getAxisRight().setDrawLabels(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.TOP);
        if(equity.getYMin()<0) {
            chart.getAxisRight().setAxisMinimum(equity.getYMin()-10);
            chart.getAxisLeft().setAxisMinimum(equity.getYMin()-10);
        }
        else {
            chart.getAxisRight().setAxisMinimum(-10);
            chart.getAxisLeft().setAxisMinimum(-10);
        }
        chart.setScaleEnabled(false);

        chart.setData(lineData);
        chart.setMarker(new LineGraphMarker(getApplicationContext(),
                R.layout.line_graph_marker,equity.getEntryCount(),chart.getYMax()));
        chart.invalidate();
    }

    private static void syncEquityChartPdf(ProcessedReport report, LineChart chart) {
        LineDataSet equity = generateEquityDataset(report);
        LineDataSet propertyValue = generatePropertyValueDataset(report);
        LineData lineData = new LineData(equity,propertyValue);

        if(!report.getRawReport().cashPurchase){
            LineDataSet loanBalance = generateLoanBalanceDataset(report);
            lineData.addDataSet(loanBalance);
        }

        chart.getDescription().setEnabled(false);

        chart.getLegend().setForm(Legend.LegendForm.CIRCLE);
        chart.getLegend().setXEntrySpace(8f);
        chart.getLegend().setWordWrapEnabled(true);
        chart.setExtraBottomOffset(8);

        chart.getXAxis().setAvoidFirstLastClipping(true);
        chart.getAxisRight().setDrawLabels(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.TOP);
        if(equity.getYMin()<0) {
            chart.getAxisRight().setAxisMinimum(equity.getYMin()-10);
            chart.getAxisLeft().setAxisMinimum(equity.getYMin()-10);
        }
        else {
            chart.getAxisRight().setAxisMinimum(-10);
            chart.getAxisLeft().setAxisMinimum(-10);
        }
        chart.setScaleEnabled(false);

        chart.setData(lineData);

        chart.invalidate();
    }

    private void generateAssumptionMessageEquity(ProcessedReport report) {
        Format format = new DecimalFormat("##0.00");
        String assumptionMessage="Assuming Property Value inflation of "+
                format.format(report.getRawReport().propertyValueInflation)+"%";
        ((TextView)findViewById(R.id.assumption_text_equity))
                .setText(assumptionMessage);
    }

    private void generateAssumptionMessageCashflow(ProcessedReport report) {
        Format format = new DecimalFormat("##0.00");
        String assumptionMessage="Assuming Income inflation of "+
                format.format(report.getRawReport().incomeInflation) +
                "% and Expenses inflation of " +
                format.format(report.getRawReport().expensesInflation) + "%";

        ((TextView)findViewById(R.id.assumption_text_cashflow))
                .setText(assumptionMessage);
    }

    private static LineDataSet generateReturnOnInvestmentDataset(ProcessedReport report){
        return generateLineDataset(report.getYearlyReturnOnInvestment(),
                "Return on Investment",Color.GREEN);
    }

    private static LineDataSet generateLoanBalanceDataset(ProcessedReport report) {
        return generateLineDataset(report.getLoanBalance(),
                "Loan Balance",Color.RED);
    }

    private static LineDataSet generatePropertyValueDataset(ProcessedReport report) {
        return generateLineDataset(report.getPropertyValue(),
                "Property Value",Color.GREEN);
    }

    private static LineDataSet generateEquityDataset(ProcessedReport report) {
        return generateLineDataset(report.getEquity(),"Equity",Color.BLUE);
    }

    private void syncCashflowGraphCard(ProcessedReport report) {
        syncCashflowChart(report,(LineChart)findViewById(R.id.cashflow_graph_chart));
        generateAssumptionMessageCashflow(report);
    }

    private void syncCashflowChart(ProcessedReport report, LineChart chart) {
        LineDataSet cashflow = generateCashflowDataset(report);
        LineDataSet income = generateLineIncomeDataset(report);
        LineDataSet expenses = generateLineExpensesDataset(report);

        LineData lineData = new LineData(income,expenses,cashflow);

        chart.getDescription().setEnabled(false);

        chart.getLegend().setForm(Legend.LegendForm.CIRCLE);
        chart.getLegend().setXEntrySpace(8f);
        chart.getLegend().setWordWrapEnabled(true);
        chart.setExtraBottomOffset(8);

        chart.getXAxis().setAvoidFirstLastClipping(true);
        chart.getAxisRight().setDrawLabels(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.TOP);
        chart.setScaleEnabled(false);

        chart.setData(lineData);

        chart.setMarker(new LineGraphMarker(getApplicationContext(),
                R.layout.line_graph_marker,cashflow.getEntryCount(),
                chart.getYMax()));
        chart.invalidate();
    }

    private static void syncCashflowChartPdf(ProcessedReport report, LineChart chart) {
        LineDataSet cashflow = generateCashflowDataset(report);
        LineDataSet income = generateLineIncomeDataset(report);
        LineDataSet expenses = generateLineExpensesDataset(report);

        LineData lineData = new LineData(income,expenses,cashflow);

        chart.getDescription().setEnabled(false);

        chart.getLegend().setForm(Legend.LegendForm.CIRCLE);
        chart.getLegend().setXEntrySpace(8f);
        chart.getLegend().setWordWrapEnabled(true);
        chart.setExtraBottomOffset(8);

        chart.getXAxis().setAvoidFirstLastClipping(true);
        chart.getAxisRight().setDrawLabels(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.TOP);
        chart.setScaleEnabled(false);

        chart.setData(lineData);

        chart.invalidate();
    }

    private static LineDataSet generateLineExpensesDataset(ProcessedReport report) {
        return generateLineDataset(report.getYearlyAnnualExpenses(),
                "Annual Expenses",Color.RED);
    }

    private static LineDataSet generateLineDataset(double[] data,String label,int color){
        List<Entry> entryList = new LinkedList<>();
        for(int i=0;i<data.length;i++){
            entryList.add(new Entry(i+1,(float)data[i]));
        }
        LineDataSet dataSet = new LineDataSet(entryList,label);
        dataSet.setFillColor(color);
        dataSet.setColor(color);
        dataSet.setLineWidth(1);
        dataSet.setDrawCircles(false);
        dataSet.setDrawValues(false);
        dataSet.setDrawFilled(true);
        return dataSet;
    }

    private static LineDataSet generateLineIncomeDataset(ProcessedReport report) {
        return generateLineDataset(report.getYearlyAnnualIncome(),
                "Annual Income",Color.GREEN);
    }

    private static LineDataSet generateCashflowDataset(ProcessedReport report) {
        return generateLineDataset(report.getYearlyAnnualCashflow(),
                "Annual Cashflow",Color.BLUE);
    }

    private static void syncExpensesPie(ProcessedReport report, PieChart expensesPie) {
        PieDataSet dataSet = generateDataForExpensesPie(report);

        dataSet.setDrawValues(false);

        PieData pieData = new PieData(dataSet);
        pieData.setValueTextSize(10);
        pieData.setValueTextColor(Color.BLACK);

        expensesPie.getLegend().setCustom(generateExpensesLegend(report));
        Legend legend = expensesPie.getLegend();
        legend.setXEntrySpace(8f);
        legend.setWordWrapEnabled(true);
        legend.setForm(Legend.LegendForm.CIRCLE);

        //set legend before data or it breaks rendering due to library bug
        expensesPie.setData(pieData);
        expensesPie.setHoleRadius(40);
        expensesPie.setTransparentCircleRadius(45);
        expensesPie.setDrawEntryLabels(false);

        expensesPie.setCenterText("Expenses");
        expensesPie.setCenterTextTypeface(Typeface
                .create("sans-serif-thin", Typeface.NORMAL));
        expensesPie.setCenterTextSize(24);

        expensesPie.setEntryLabelColor(Color.BLACK);
        expensesPie.setEntryLabelTypeface
                (Typeface.create(Typeface.SANS_SERIF,Typeface.BOLD));
        expensesPie.setEntryLabelTextSize(13);

        expensesPie.getDescription().setEnabled(false);
        //TODO do Final touch up and make permanent colours for both this and income
        expensesPie.invalidate();
    }

    private static List<LegendEntry> generateExpensesLegend(ProcessedReport report) {
        List<LegendEntry> list = new LinkedList<>();
        Format format = new DecimalFormat("################0.00");
        LegendEntry val = makeLegendEntry
                ("Management - " + calculateManagementCosts(report.getRawReport())
                        , MANAGEMENT_COLOR);
        list.add(val);
        val = makeLegendEntry
                ("Vacancy - " + format.format(calculateVacancyCosts(report.getRawReport()))
                        ,VACANCY_COLOR);
        list.add(val);
        val = makeLegendEntry
                ("Maintenance - " + format.format(calculateMaintenanceCosts(report.getRawReport()))
                        , MAINTENANCE_COLOR);
        list.add(val);
        val = makeLegendEntry
                ("Cap Ex - " + format.format(calculateCapEx(report.getRawReport()))
                        ,CAP_EX_COLOR);
        list.add(val);
        val = makeLegendEntry("Electricity - " + format.format(report.getRawReport().electricity),
                ELECTRICITY_COLOR);
        list.add(val);
        val = makeLegendEntry("Water - " + format.format(report.getRawReport().water),
                WATER_COLOR);
        list.add(val);
        val = makeLegendEntry("Sewer - " + format.format(report.getRawReport().sewer),
                SEWER_COLOR);
        list.add(val);
        val = makeLegendEntry("Insurance - " + format.format(report.getRawReport().insurance),
                INSURANCE_COLOR);
        list.add(val);
        val = makeLegendEntry("Other - " + format.format(report.getRawReport().otherExpenses),
                OTHER_EXPENSES_COLOR);
        list.add(val);
        val = makeLegendEntry("HOA - " + format.format(report.getRawReport().hoa),
                HOA_COLOR);
        list.add(val);
        val = makeLegendEntry("Loan Payment - " + format.format(report.getLoanMonthlyPayment()),
                LOAN_PAYMENT_COLOR);
        list.add(val);
        val = makeLegendEntry("Property tax - " + format.format(report.getRawReport().propertyTax),
                PROPERTY_TAX_COLOR);
        list.add(val);
        return list;

    }

    private static LegendEntry makeLegendEntry(String label, int color) {
        LegendEntry val = new LegendEntry();
        val.label = label;
        val.formColor = color;
        return val;
    }

    private static PieDataSet generateDataForExpensesPie(ProcessedReport report) {
        //Expenses = operating expenses + loan payment
        //Operating expenses = management + maintenance +
        // cap ex + water + electricity + hoa + insurance + sewer + other
        PieDataSet dataSet = new PieDataSet(new LinkedList<PieEntry>(),"");
        dataSet.resetColors();//to clear default first color
        addPieEntryToListIfNotZero
                (dataSet, calculateManagementCosts(report.getRawReport()),
                        "Management",MANAGEMENT_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, calculateVacancyCosts(report.getRawReport()),
                        "Vacancy", VACANCY_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, calculateMaintenanceCosts(report.getRawReport()),
                        "Maintenance",MAINTENANCE_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, calculateCapEx(report.getRawReport()), "Cap Ex" ,
                        CAP_EX_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, report.getRawReport().electricity, "Electricity",
                        ELECTRICITY_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, report.getRawReport().water, "Water", WATER_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, report.getRawReport().sewer, "Sewer", SEWER_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, report.getRawReport().insurance, "Insurance",
                        INSURANCE_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, report.getRawReport().otherExpenses, "Other", OTHER_EXPENSES_COLOR);
        addPieEntryToListIfNotZero
                (dataSet, report.getRawReport().hoa, "HOA",HOA_COLOR);

        addPieEntryToListIfNotZero(dataSet,report.getLoanMonthlyPayment(),
                "Loan Payment", LOAN_PAYMENT_COLOR);

        addPieEntryToListIfNotZero(dataSet,report.getRawReport().propertyTax,
                "Property Tax",PROPERTY_TAX_COLOR);
        return dataSet;
    }

    private static void addPieEntryToListIfNotZero(PieDataSet dataSet, double val, String label, int color){
        if(val!=0){
            dataSet.addEntry(new PieEntry((float)val,label));
            dataSet.addColor(color);
        }
    }

    private static void syncIncomePie(FullReport report, PieChart incomePie) {
        List<PieEntry> dataList = new LinkedList<>();
        dataList.add(new PieEntry(report.rent,"Rent"));
        dataList.add(new PieEntry(report.otherIncome,"Other"));

        PieDataSet dataSet = new PieDataSet(dataList,"");
        dataSet.setColors(RENT_COLOR, OTHER_INCOME_COLOR); //green, dark violet
        dataSet.setDrawValues(false);
        PieData pieData = new PieData(dataSet);

        incomePie.getLegend().setCustom(generateIncomeLegend(report));
        incomePie.getLegend().setTextSize(12);
        incomePie.getLegend().setForm(Legend.LegendForm.CIRCLE);
        incomePie.getLegend().setWordWrapEnabled(true);
        //set legend before data or it breaks rendering due to library bug
        incomePie.setData(pieData);
        incomePie.setDrawEntryLabels(false);
        incomePie.setHoleRadius(40);
        incomePie.setTransparentCircleRadius(45);

        incomePie.setCenterText("Income");
        incomePie.setCenterTextTypeface(Typeface
                .create("sans-serif-thin", Typeface.NORMAL));
        incomePie.setCenterTextSize(24);

        incomePie.getDescription().setEnabled(false);

    }

    private static List<LegendEntry> generateIncomeLegend(FullReport report) {
        List<LegendEntry> list = new LinkedList<>();
        Format format = new DecimalFormat("################0.00");
        LegendEntry val = makeLegendEntry
                ("Rent - " + format.format(report.rent), RENT_COLOR);
        list.add(val);
        val = makeLegendEntry
                ("Other - " + format.format(report.otherExpenses), OTHER_INCOME_COLOR);
        list.add(val);
        return list;
    }

    private void syncFinancialOverviewCard(ProcessedReport processedReport) {
        ((TextView)findViewById(R.id.monthly_income))
                .setText(Utils.formatToMoney(calculateMonthlyIncome
                        (processedReport.getRawReport())));

        ((TextView)findViewById(R.id.monthly_expenses))
                .setText(Utils.formatToMoney(processedReport.getMonthlyExpenses()));

        ((TextView)findViewById(R.id.monthly_cashflow))
                .setText(Utils.formatToMoney(processedReport.getMonthlyCashflow()));

        ((TextView)findViewById(R.id.noi))
                .setText(Utils.formatToMoney(calculateNetOperatingIncome
                        (processedReport.getRawReport())));

        ((TextView)findViewById(R.id.cash_on_cash_return))
                .setText(Utils.formatToPercent(processedReport.getCashOnCashReturn()));

        ((TextView)findViewById(R.id.total_cash_needed))
                .setText(Utils.formatToMoney(processedReport.getTotalCashNeeded()));

        ((TextView)findViewById(R.id.pro_forma_cap_rate))
                .setText(Utils.formatToPercent(getProFormaCapRate
                        (processedReport.getRawReport())));

        ((TextView)findViewById(R.id.purchase_cap_rate))
                .setText(Utils.formatToPercent(getPurchaseCapRate
                        (processedReport.getRawReport())));
    }

    private void syncTitleCard(FullReport report) {
        syncReportImage(report.imageURI);

        ((TextView)findViewById(R.id.report_name)).setText(report.reportName);
        ((TextView)findViewById(R.id.address)).setText(report.address);
        ((TextView)findViewById(R.id.zip)).setText(report.zip);
        ((TextView)findViewById(R.id.city)).setText(report.city);
        ((TextView)findViewById(R.id.description)).setText(report.miscText);
    }

    private void syncReportImage(String imageURI) {
        model.setReportImage
                (Utils.getDrawableFromURI
                        (getApplicationContext(),
                                Utils.resolveUriFromString(imageURI)));

        ((ImageView)findViewById(R.id.report_image)).setImageDrawable(model.getReportImage());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_report);

        attachHelpListeners();

        long reportID=this.getIntent().getLongExtra(ID,-1);
        if(reportID==-1){
            throw new InvalidParameterException("No ID put in starting Intent");
        }

        findViewById(R.id.expand_button_title).setTag(1);
        findViewById(R.id.expand_button_financial_overview).setTag(1);
        findViewById(R.id.expand_button_income).setTag(1);
        findViewById(R.id.expand_button_expenses).setTag(1);
        findViewById(R.id.expand_button_cashflow_graph).setTag(1);
        findViewById(R.id.expand_button_equity_graph).setTag(1);
        findViewById(R.id.expand_button_roi_graph).setTag(1);
        findViewById(R.id.expand_button_loan_details).setTag(1);

        findViewById(R.id.pdf_button).setEnabled(false);

        model= ViewModelProviders.of(this,
                new DisplayActivityViewModelFactory(getApplication(),reportID))
                .get(DisplayActivityViewModel.class);

        model.getRawReport().observe(this,rawReportObserver);
        model.getProcessedReport().observe(this,processedReportObserver);
    }

    private void attachHelpListeners() {
        findViewById(R.id.monthly_cashflow_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.monthly_cashflow_help_title),
                        getResources().getString(R.string.monthly_cashflow_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.total_cash_needed_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.total_cash_needed_help_title),
                        getResources().getString(R.string.total_cash_needed_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.noi_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.noi_help_title),
                        getResources().getString(R.string.noi_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.cash_on_cash_return_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.cash_on_cash_return_help_title),
                        getResources().getString(R.string.cash_on_cash_return_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.pro_forma_cap_rate_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.pro_forma_cap_rate_help_title),
                        getResources().getString(R.string.pro_forma_cap_rate_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.purchase_cap_rate_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.purchase_cap_rate_help_title),
                        getResources().getString(R.string.purchase_cap_rate_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.equity_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.equity_help_title),
                        getResources().getString(R.string.equity_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.roi_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.roi_help_title),
                        getResources().getString(R.string.roi_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.repayment_amount_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.repayment_amount_help_title),
                        getResources().getString(R.string.repayment_amount_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.dept_coverage_ratio_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.dept_coverage_ratio_help_title),
                        getResources().getString(R.string.dept_coverage_ratio_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.dept_coverage_ratio_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.dept_coverage_ratio_help_title),
                        getResources().getString(R.string.dept_coverage_ratio_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.loan_amount_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.loan_amount_help_title),
                        getResources().getString(R.string.loan_amount_help_message),
                        getSupportFragmentManager()));

        findViewById(R.id.loan_payment_help).setOnClickListener
                (new DisplayMessageOnClickListener(
                        getResources().getString(R.string.monthly_loan_payment_help_title),
                        getResources().getString(R.string.monthly_loan_payment_help_message),
                        getSupportFragmentManager()));
    }

    public void processTitleButtonPress(View view){
        if((Integer)view.getTag()==0){
            expandTitleCard();
            performExpandAnimation(view);
            view.setTag(1);
        }
        else {
            collapseTitleCard();
            performCollapseAnimation(view);
            view.setTag(0);
        }
    }

    private void performCollapseAnimation(View view) {
        VectorDrawable icon = (VectorDrawable) getResources()
                .getDrawable(R.drawable.ic_round_expand_more_24px,this.getTheme());

        ((Button)view).setCompoundDrawablesRelativeWithIntrinsicBounds
                (icon,null,null,null);
        ((TextView)view).setText(getResources().getString(R.string.Expand));
    }

    private void performExpandAnimation(View view) {
        VectorDrawable icon = (VectorDrawable) getResources()
                .getDrawable(R.drawable.ic_round_expand_less_24px,this.getTheme());

        ((Button)view).setCompoundDrawablesRelativeWithIntrinsicBounds
                (icon,null,null,null);
        ((TextView)view).setText(getResources().getString(R.string.Collapse));
    }

    private void collapseTitleCard(){
        findViewById(R.id.address_label).setVisibility(View.GONE);
        findViewById(R.id.address).setVisibility(View.GONE);
        findViewById(R.id.city_label).setVisibility(View.GONE);
        findViewById(R.id.city).setVisibility(View.GONE);
        findViewById(R.id.zip_label).setVisibility(View.GONE);
        findViewById(R.id.zip).setVisibility(View.GONE);
        findViewById(R.id.description_label).setVisibility(View.GONE);
        findViewById(R.id.description).setVisibility(View.GONE);
        findViewById(R.id.divider).setVisibility(View.GONE);
    }

    private void expandTitleCard(){
        findViewById(R.id.address_label).setVisibility(View.VISIBLE);
        findViewById(R.id.address).setVisibility(View.VISIBLE);
        findViewById(R.id.city_label).setVisibility(View.VISIBLE);
        findViewById(R.id.city).setVisibility(View.VISIBLE);
        findViewById(R.id.zip_label).setVisibility(View.VISIBLE);
        findViewById(R.id.zip).setVisibility(View.VISIBLE);
        findViewById(R.id.description_label).setVisibility(View.VISIBLE);
        findViewById(R.id.description).setVisibility(View.VISIBLE);
        findViewById(R.id.divider).setVisibility(View.VISIBLE);
    }

    public void processFinancialOverviewButtonPress(View view){
        if((Integer)view.getTag()==0){
            expandFinancialOverviewCard();
            performExpandAnimation(view);
            view.setTag(1);
        }
        else {
            collapseFinancialOverviewCard();
            performCollapseAnimation(view);
            view.setTag(0);
        }
    }

    private void expandFinancialOverviewCard(){
        findViewById(R.id.monthly_income_label).setVisibility(View.VISIBLE);
        findViewById(R.id.monthly_income).setVisibility(View.VISIBLE);
        findViewById(R.id.monthly_expenses_label).setVisibility(View.VISIBLE);
        findViewById(R.id.monthly_expenses).setVisibility(View.VISIBLE);
        findViewById(R.id.monthly_cashflow_label).setVisibility(View.VISIBLE);
        findViewById(R.id.monthly_cashflow).setVisibility(View.VISIBLE);
        findViewById(R.id.monthly_cashflow_help).setVisibility(View.VISIBLE);
        findViewById(R.id.total_cash_needed_label).setVisibility(View.VISIBLE);
        findViewById(R.id.total_cash_needed).setVisibility(View.VISIBLE);
        findViewById(R.id.total_cash_needed_help).setVisibility(View.VISIBLE);
        findViewById(R.id.cash_on_cash_return_label).setVisibility(View.VISIBLE);
        findViewById(R.id.cash_on_cash_return).setVisibility(View.VISIBLE);
        findViewById(R.id.cash_on_cash_return_help).setVisibility(View.VISIBLE);
        findViewById(R.id.noi_label).setVisibility(View.VISIBLE);
        findViewById(R.id.noi).setVisibility(View.VISIBLE);
        findViewById(R.id.noi_help).setVisibility(View.VISIBLE);
        findViewById(R.id.pro_forma_cap_rate_label).setVisibility(View.VISIBLE);
        findViewById(R.id.pro_forma_cap_rate).setVisibility(View.VISIBLE);
        findViewById(R.id.pro_forma_cap_rate_help).setVisibility(View.VISIBLE);
        findViewById(R.id.purchase_cap_rate_label).setVisibility(View.VISIBLE);
        findViewById(R.id.purchase_cap_rate).setVisibility(View.VISIBLE);
        findViewById(R.id.purchase_cap_rate_help).setVisibility(View.VISIBLE);
    }

    private void collapseFinancialOverviewCard(){
        findViewById(R.id.monthly_income_label).setVisibility(View.GONE);
        findViewById(R.id.monthly_income).setVisibility(View.GONE);
        findViewById(R.id.monthly_expenses_label).setVisibility(View.GONE);
        findViewById(R.id.monthly_expenses).setVisibility(View.GONE);
        findViewById(R.id.monthly_cashflow_label).setVisibility(View.GONE);
        findViewById(R.id.monthly_cashflow).setVisibility(View.GONE);
        findViewById(R.id.monthly_cashflow_help).setVisibility(View.GONE);
        findViewById(R.id.total_cash_needed_label).setVisibility(View.GONE);
        findViewById(R.id.total_cash_needed).setVisibility(View.GONE);
        findViewById(R.id.total_cash_needed_help).setVisibility(View.GONE);
        findViewById(R.id.cash_on_cash_return_label).setVisibility(View.GONE);
        findViewById(R.id.cash_on_cash_return).setVisibility(View.GONE);
        findViewById(R.id.cash_on_cash_return_help).setVisibility(View.GONE);
        findViewById(R.id.noi_label).setVisibility(View.GONE);
        findViewById(R.id.noi).setVisibility(View.GONE);
        findViewById(R.id.noi_help).setVisibility(View.GONE);
        findViewById(R.id.pro_forma_cap_rate_label).setVisibility(View.GONE);
        findViewById(R.id.pro_forma_cap_rate).setVisibility(View.GONE);
        findViewById(R.id.pro_forma_cap_rate_help).setVisibility(View.GONE);
        findViewById(R.id.purchase_cap_rate_label).setVisibility(View.GONE);
        findViewById(R.id.purchase_cap_rate).setVisibility(View.GONE);
        findViewById(R.id.purchase_cap_rate_help).setVisibility(View.GONE);
    }

    public void processIncomeButtonPress(View view){
        if((Integer)view.getTag()==0){
            expandIncomeCard();
            performExpandAnimation(view);
            view.setTag(1);
        }
        else {
            collapseIncomeCard();
            performCollapseAnimation(view);
            view.setTag(0);
        }
    }

    public void processExpensesButtonPress(View view){
        if((Integer)view.getTag()==0){
            expandExpensesCard();
            performExpandAnimation(view);
            view.setTag(1);
        }
        else {
            collapseExpensesCard();
            performCollapseAnimation(view);
            view.setTag(0);
        }
    }

    public void processCashflowGraphButtonPress(View view){
        if((Integer)view.getTag()==0){
            expandCashflowGraphCard();
            performExpandAnimation(view);
            view.setTag(1);
        }
        else {
            collapseCashflowGraphCard();
            performCollapseAnimation(view);
            view.setTag(0);
        }
    }

    public void processEquityGraphButtonPress(View view){
        if((Integer)view.getTag()==0){
            expandEquityGraphCard();
            performExpandAnimation(view);
            view.setTag(1);
        }
        else {
            collapseEquityGraphCard();
            performCollapseAnimation(view);
            view.setTag(0);
        }
    }

    public void processLoanDetailsButtonPress(View view){
        if((Integer)view.getTag()==0){
            expandLoanDetailsCard();
            performExpandAnimation(view);
            view.setTag(1);
        }
        else {
            collapseLoanDetailsCard();
            performCollapseAnimation(view);
            view.setTag(0);
        }
    }

    private void collapseLoanDetailsCard() {
        findViewById(R.id.loan_payment).setVisibility(View.GONE);
        findViewById(R.id.loan_payment_help).setVisibility(View.GONE);
        findViewById(R.id.loan_payment_label).setVisibility(View.GONE);
        findViewById(R.id.loan_amount).setVisibility(View.GONE);
        findViewById(R.id.loan_amount_help).setVisibility(View.GONE);
        findViewById(R.id.loan_amount_label).setVisibility(View.GONE);
        findViewById(R.id.dept_coverage_ratio).setVisibility(View.GONE);
        findViewById(R.id.dept_coverage_ratio_help).setVisibility(View.GONE);
        findViewById(R.id.dept_coverage_ratio_label).setVisibility(View.GONE);
        findViewById(R.id.repayment_amount).setVisibility(View.GONE);
        findViewById(R.id.repayment_amount_help).setVisibility(View.GONE);
        findViewById(R.id.repayment_amount_label).setVisibility(View.GONE);
    }

    private void expandLoanDetailsCard() {
        findViewById(R.id.loan_payment).setVisibility(View.VISIBLE);
        findViewById(R.id.loan_payment_help).setVisibility(View.VISIBLE);
        findViewById(R.id.loan_payment_label).setVisibility(View.VISIBLE);
        findViewById(R.id.loan_amount).setVisibility(View.VISIBLE);
        findViewById(R.id.loan_amount_help).setVisibility(View.VISIBLE);
        findViewById(R.id.loan_amount_label).setVisibility(View.VISIBLE);
        findViewById(R.id.dept_coverage_ratio).setVisibility(View.VISIBLE);
        findViewById(R.id.dept_coverage_ratio_help).setVisibility(View.VISIBLE);
        findViewById(R.id.dept_coverage_ratio_label).setVisibility(View.VISIBLE);
        findViewById(R.id.repayment_amount).setVisibility(View.VISIBLE);
        findViewById(R.id.repayment_amount_help).setVisibility(View.VISIBLE);
        findViewById(R.id.repayment_amount_label).setVisibility(View.VISIBLE);
    }

    public void processRoiGraphButtonPress(View view){
        if((Integer)view.getTag()==0){
            expandRoiGraphCard();
            performExpandAnimation(view);
            view.setTag(1);
        }
        else {
            collapseRoiGraphCard();
            performCollapseAnimation(view);
            view.setTag(0);
        }
    }

    private void collapseRoiGraphCard() {
        findViewById(R.id.roi_graph_chart).setVisibility(View.GONE);
        findViewById(R.id.years_label_roi).setVisibility(View.GONE);
        findViewById(R.id.assumption_text_roi).setVisibility(View.GONE);
    }

    private void expandRoiGraphCard() {
        findViewById(R.id.roi_graph_chart).setVisibility(View.VISIBLE);
        findViewById(R.id.years_label_roi).setVisibility(View.VISIBLE);
        findViewById(R.id.assumption_text_roi).setVisibility(View.VISIBLE);
    }

    private void collapseEquityGraphCard() {
        findViewById(R.id.equity_graph_chart).setVisibility(View.GONE);
        findViewById(R.id.years_label_equity).setVisibility(View.GONE);
        findViewById(R.id.assumption_text_equity).setVisibility(View.GONE);
        findViewById(R.id.equity_help).setVisibility(View.GONE);
    }

    private void expandEquityGraphCard() {
        findViewById(R.id.equity_graph_chart).setVisibility(View.VISIBLE);
        findViewById(R.id.years_label_equity).setVisibility(View.VISIBLE);
        findViewById(R.id.assumption_text_equity).setVisibility(View.VISIBLE);
        findViewById(R.id.equity_help).setVisibility(View.VISIBLE);
    }

    private void collapseCashflowGraphCard() {
        findViewById(R.id.cashflow_graph_chart).setVisibility(View.GONE);
        findViewById(R.id.years_label).setVisibility(View.GONE);
        findViewById(R.id.assumption_text_cashflow).setVisibility(View.GONE);

    }

    private void expandCashflowGraphCard() {
        findViewById(R.id.cashflow_graph_chart).setVisibility(View.VISIBLE);
        findViewById(R.id.years_label).setVisibility(View.VISIBLE);
        findViewById(R.id.assumption_text_cashflow).setVisibility(View.VISIBLE);
    }

    private void collapseExpensesCard() {
        findViewById(R.id.expenses_pie).setVisibility(View.GONE);
    }

    private void expandExpensesCard() {
        findViewById(R.id.expenses_pie).setVisibility(View.VISIBLE);
    }

    private void collapseIncomeCard() {
        findViewById(R.id.income_pie).setVisibility(View.GONE);
    }

    private void expandIncomeCard() {
        findViewById(R.id.income_pie).setVisibility(View.VISIBLE);
    }

    public void launchEditActivity(View view){
        long id=getIntent().getLongExtra(ID,-1);
        Intent intent = new Intent(this, DataEntryActivity.class);
        intent.putExtra(MODE,EDIT);
        intent.putExtra(DataEntryActivity.ID,id);
        startActivity(intent);
    }

    private static PdfDocument exportToPDF(LayoutInflater inflater, ProcessedReport report,
                                           Drawable reportImage, Resources resources){
        PdfDocument document = new PdfDocument();
        PdfDocument.PageInfo pageInfo = new PdfDocument.PageInfo
                .Builder(595,842,1).create();
        PdfDocument.Page page = document.startPage(pageInfo);
        Canvas canvas = page.getCanvas();

        float ptScale = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_PT,1,
                Resources.getSystem().getDisplayMetrics());

        canvas.scale(1/ptScale,1/ptScale);

        View topContainer = inflater
                .inflate(R.layout.pdf_title_page,null,false);

        int measureWidth = View.MeasureSpec.makeMeasureSpec
                ((int)(page.getCanvas().getWidth()*ptScale), View.MeasureSpec.EXACTLY);

        int measuredHeight = View.MeasureSpec.makeMeasureSpec
                ((int)(page.getCanvas().getHeight()*ptScale), View.MeasureSpec.EXACTLY);

        topContainer.measure(measureWidth, measuredHeight);
        topContainer.layout(0, 0, topContainer.getMeasuredWidth(),
                topContainer.getMeasuredHeight());

        fillPdfLayoutTitlePage(report, reportImage, topContainer, resources);

        canvas.setDensity((int)Resources.getSystem().getDisplayMetrics().density);//force non-gpu rendering

        topContainer.draw(canvas);

        document.finishPage(page);

        pageInfo = new PdfDocument.PageInfo
                .Builder(595,842,1).create();
        page = document.startPage(pageInfo);
        canvas = page.getCanvas();

        canvas.scale(1/ptScale,1/ptScale);

        topContainer = inflater.inflate(R.layout.pdf_financial_page,null,false);

        measureWidth = View.MeasureSpec.makeMeasureSpec
                ((int)(page.getCanvas().getWidth()*ptScale), View.MeasureSpec.EXACTLY);

        measuredHeight = View.MeasureSpec.makeMeasureSpec
                ((int)(page.getCanvas().getHeight()*ptScale), View.MeasureSpec.EXACTLY);

        topContainer.measure(measureWidth, measuredHeight);
        topContainer.layout(0, 0, topContainer.getMeasuredWidth(),
                topContainer.getMeasuredHeight());

        fillPdfLayoutFinancialPage(report, topContainer, resources);

        canvas.setDensity((int)Resources.getSystem().getDisplayMetrics().density);//force non-gpu rendering

        topContainer.draw(canvas);

        document.finishPage(page);

        return document;
    }

    private static boolean savePdfToStorage(PdfDocument document, Uri uri, ContentResolver contentResolver){
        boolean fileSaved = false;
        try (OutputStream outputStream = contentResolver.openOutputStream(uri)) {
            document.writeTo(outputStream);
            fileSaved = true;
        } catch (IOException e) {
            Log.e("FL", e.getMessage());
            Log.e("FL", Arrays.toString(e.getStackTrace()));
        } catch (NullPointerException e){
            Log.e("FL", e.getMessage());
            Log.e("FL", Arrays.toString(e.getStackTrace()));
        } finally {
            document.close();
        }
        return fileSaved;
    }

    private static void fillPdfLayoutTitlePage(ProcessedReport report, Drawable reportImage,
                                               View topContainer, Resources resources) {
        ((ImageView)topContainer.findViewById(R.id.report_image))
                .setImageDrawable(reportImage);
        syncExpensesPie(report, (PieChart) topContainer.findViewById(R.id.expenses_pie));

        syncIncomePie(report.getRawReport(),
                (PieChart) topContainer.findViewById(R.id.income_pie));

        ((TextView)topContainer.findViewById(R.id.report_name))
                .setText(report.getRawReport().reportName);

        ((TextView)topContainer.findViewById(R.id.address))
                .setText(report.getRawReport().address);

        //hack as android does not calculate text size properly for large text
        ((TextView)topContainer.findViewById(R.id.monthly_income))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getMonthlyIncome())));

        ((TextView)topContainer.findViewById(R.id.monthly_expenses))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getMonthlyExpenses())));

        ((TextView)topContainer.findViewById(R.id.monthly_cashflow))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getMonthlyCashflow())));

        ((TextView)topContainer.findViewById(R.id.total_cash_needed))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getTotalCashNeeded())));

        ((TextView)topContainer.findViewById(R.id.noi))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getNetOperatingIncome())));

        ((TextView)topContainer.findViewById(R.id.cash_on_cash_return))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getCashOnCashReturn())));

        ((TextView)topContainer.findViewById(R.id.purchase_price))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getRawReport().purchasePrice)));

        ((TextView)topContainer.findViewById(R.id.closing_costs))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getRawReport().closingCosts)));

        ((TextView)topContainer.findViewById(R.id.after_repair_value))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getRawReport().afterRepairValue)));

        ((TextView)topContainer.findViewById(R.id.repair_costs))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getRawReport().repairCosts)));

        ((TextView)topContainer.findViewById(R.id.loan_amount))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getRawReport().loanAmount)));

        ((TextView)topContainer.findViewById(R.id.interest_rate))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().interestRate)));

        ((TextView)topContainer.findViewById(R.id.lender_fees))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getRawReport().lenderCharges)));

        ((TextView)topContainer.findViewById(R.id.amortization_time))
                .setText(String.format("%s years ",
                        Utils.formatToMoney(report.getRawReport().amortizationRate)));

        ((TextView)topContainer.findViewById(R.id.points_charges))
                .setText(String.format("%s ",
                        Utils.formatToMoney(report.getRawReport().pointCharges)));

        ((TextView)topContainer.findViewById(R.id.pro_forma_cap_rate))
                .setText(String.format("%s ",
                        Utils.formatToPercent
                                (getProFormaCapRate(report.getRawReport()))));

        ((TextView)topContainer.findViewById(R.id.description))
                .setText(String.format("%s ", report.getRawReport().miscText));

        ((TextView)topContainer.findViewById(R.id.purchase_cap_rate))
                .setText(String.format("%s ",
                        Utils.formatToPercent
                                (getPurchaseCapRate(report.getRawReport()))));

        ((TextView)topContainer.findViewById(R.id.typical_cap_rate))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().capRate)));

        ((ImageView)topContainer.findViewById(R.id.income_pie_image))
                .setImageDrawable(new BitmapDrawable(resources,
                        ((PieChart)topContainer.findViewById(R.id.income_pie))
                                .getChartBitmap()));

        ((ImageView)topContainer.findViewById(R.id.expenses_pie_image))
                .setImageDrawable(new BitmapDrawable(resources,
                        ((PieChart)topContainer.findViewById(R.id.expenses_pie))
                                .getChartBitmap()));
    }

    private static void fillPdfLayoutFinancialPage(ProcessedReport report , View topContainer,
                                                   Resources resources){
        syncCashflowChartPdf(report, (LineChart) topContainer.findViewById(R.id.cashflow_chart));

        syncRoiChartPdf(report, (LineChart) topContainer.findViewById(R.id.roi_chart));

        syncEquityChartPdf(report, (LineChart) topContainer.findViewById(R.id.equity_chart));

        //hack as android does not calculate text size properly for large text
        ((TextView)topContainer.findViewById(R.id.income_inflation))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().incomeInflation)));

        ((TextView)topContainer.findViewById(R.id.expenses_inflation))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().expensesInflation)));

        ((TextView)topContainer.findViewById(R.id.property_value_inflation))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().propertyValueInflation)));

        ((TextView)topContainer.findViewById(R.id.vacancy))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().vacancy)));

        ((TextView)topContainer.findViewById(R.id.management))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().management)));

        ((TextView)topContainer.findViewById(R.id.maintenance))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().maintenance)));

        ((TextView)topContainer.findViewById(R.id.cap_ex))
                .setText(String.format("%s ",
                        Utils.formatToPercent(report.getRawReport().capEX)));

        ((ImageView)topContainer.findViewById(R.id.cashflow_chart_image))
                .setImageDrawable(new BitmapDrawable(resources,
                        ((LineChart)topContainer.findViewById(R.id.cashflow_chart))
                                .getChartBitmap()));

        ((ImageView)topContainer.findViewById(R.id.roi_chart_image))
                .setImageDrawable(new BitmapDrawable(resources,
                        ((LineChart)topContainer.findViewById(R.id.roi_chart))
                                .getChartBitmap()));

        ((ImageView)topContainer.findViewById(R.id.equity_chart_image))
                .setImageDrawable(new BitmapDrawable(resources,
                        ((LineChart)topContainer.findViewById(R.id.equity_chart))
                                .getChartBitmap()));
    }

    public void launchDestinationSelector(View view) {
        Intent getLocation = new Intent(ACTION_CREATE_DOCUMENT);
        getLocation.setType("application/pdf");
        startActivityForResult(getLocation,FILE_SAVE_LOCATION_ACTIVITY_CODE);
    }

    private static class ExportPdfTask extends AsyncTask<Void,Integer,Boolean> {
        private ProcessedReport report;
        private LayoutInflater layoutInflater;
        private Uri uri;
        private Drawable reportImage;
        private WeakReference<Context> contextWeakReference;
        private PdfDocument document;
        private ContentResolver contentResolver;

        private ExportPdfTask(ProcessedReport report, LayoutInflater layoutInflater,
                             Uri uri,Drawable reportImage, Context context) {
            this.report = report;
            this.layoutInflater = layoutInflater;
            this.uri = uri;
            this.reportImage = reportImage;
            this.contextWeakReference = new WeakReference<>(context);
            this.contentResolver = context.getContentResolver();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            document = exportToPDF(layoutInflater,report,reportImage,
                    contextWeakReference.get().getResources());

        }

        @Override
        protected Boolean doInBackground(Void... unused) {
            return savePdfToStorage(document,uri,contentResolver);
        }

        @Override
        protected void onPostExecute(Boolean completed) {
            super.onPostExecute(completed);
            if(completed){
                Toast.makeText(contextWeakReference.get(),
                        "Report created successfully",Toast.LENGTH_SHORT).show();
            }
            else {
                Toast.makeText(contextWeakReference.get(),
                        "Something went wrong while making report",
                        Toast.LENGTH_SHORT).show();
            }

        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == FILE_SAVE_LOCATION_ACTIVITY_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri saveLocation = null;
                if (data != null) {
                    saveLocation = data.getData();
                    if(saveLocation!=null){
                        Log.i("FL", "Uri: " + saveLocation.toString());
                    }

                    new ExportPdfTask(model.getProcessedReport().getValue(),
                            getLayoutInflater(),saveLocation
                            ,model.getReportImage(), getApplicationContext())
                            .execute();
                }
            }
        }
    }
}
