# Investment Rental Calculator

The Investment Rental Calculator is an Android app used to help new investors analyse rental properties they wish to invest in. It is a small side project I did in my spare time to help me learn Android
[Play Store Link](https://play.google.com/store/apps/details?id=com.valentin.petrov.investmentrentalcalculator)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

JDK and Android SDK

I would strongly recommend getting Android Studio if you intend to work with Android. It comes with all prerequisites for building and running and lots of useful tooling.

```
Give examples
```

### Building

For command line users just go to the project directory and run for debug .apk

```
//Linux
gradlew assembleDebug

//Windows
gradlew.bat assembleDebug
```

and for a release version

```
//Linux
gradlew assembleRelease

//Windows
gradlew.bat assembleRelease
```

If you decide to use Android Studio simply import the project into it and it will be build automatically

## Running 
If you are using Android Studio just press the run button

If you want to run it via command line (do not recommend, its very tedious)

```
<your android sdk directory>/platform-tools/adb install -r <your APK file>
```

If this doesn’t work then push the APK using ADB:

```
adb push <your android app directory>/build/outputs/apk/debug/app-debug.apk /data/local/tmp/org.tensorflow.lite.examples.classification
```

Then start launcher:
```
adb shell am start -n “org.tensorflow.lite.examples.classification/org.tensorflow.lite.examples.classification.ClassifierActivity” -a android.intent.action.MAIN -c android.intent.category.LAUNCHER
```

### Tests

Tests live in 

```
<project_folder>\app\src\test\java\com\valentin\petrov\investmentrentalcalculator

```

They are by no means comprehensive and there is no direct way of running them from command line.
They can be ran via Android Studio.

###Current coverage
*Input checking
*Report processing math

## Deployment

To deploy on a live system just send a signed .apk to device and run or download from Play Store

## Built With

* Gradle wrapper

## Authors

* **Valentin Petrov** - [LinkedIn](https://www.linkedin.com/public-profile/settings?trk=d_flagship3_profile_self_view_public_profile&lipi=urn%3Ali%3Apage%3Ad_flagship3_profile_self_edit_top_card%3BEb7UgCDRSOS7PQ1CAnPmrA%3D%3D)

## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details

## Acknowledgments

* MPAndroidCharts - for the single buggiest charting library of all time, but the best free alternative available